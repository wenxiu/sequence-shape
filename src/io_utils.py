# AUTHOR: Wenxiu Ma
# MODIFIED DATA: 18 May 2015

import os
import sys

from dnashape import *

#############################################################################
def getKmer(k, alphabet=None, dir=None):

  kmerfilename="%dmer.seq.gz" % k
  if dir is not None:
    kmerfilename = dir + "/" + kmerfilename

  if not os.path.isfile(kmerfilename):
    kmersDict,kmersList = generateKmer(k, alphabet=alphabet, outputfilename=kmerfilename)
  else:
    kmersDict,kmersList = loadKmer(kmerfilename, k=k)
  return kmersDict,kmersList

#############################################################################
def getUniqKmer(k, alphabet=None, kmersDict=None, kmersList=None, dir=None):

  uniqKmerfilename="%dmer.uniq.gz" % k
  if dir is not None:
    uniqKmerfilename = dir + "/" + uniqKmerfilename

  if not os.path.isfile(uniqKmerfilename):
    if kmersDict is None or kmersList is None:
      kmersDict,kmersList = getKmer(k, alphabet=alphabet, dir=dir)

    uniqKmersDict,uniqKmersList = generateUniqKmer(k, alphabet=alphabet,
                                                   kmersDict=kmersDict, kmersList=kmersList,
                                                   outputfilename=uniqKmerfilename)
  else:
    uniqKmersDict,uniqKmersList = loadUniqKmer(uniqKmerfilename, k=k)

  return uniqKmersDict,uniqKmersList

#############################################################################
def getKmerFeature(seqsFilename, k, seqs=None, alphabet=None,
                   uniqKmersDict=None, uniqKmersList=None,
                   kmersDict=None, kmersList=None, dir=None):

  kmerFeatureFilename="%s.%dmer.features.npz" % (seqsFilename, k)
  if dir is not None:
    kmerFeatureFilename = dir + "/" + os.path.basename(kmerFeatureFilename)

  if not os.path.isfile(kmerFeatureFilename):
    assert seqs is not None
    kmerFeatureArray = buildKmerFeature(seqs, k, alphabet=alphabet,
                                        uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                        kmersDict=kmersDict, kmersList=kmersList,
                                        outputfilename=kmerFeatureFilename)
  else:
    kmerFeatureArray = loadKmerFeature(kmerFeatureFilename)

  return kmerFeatureArray

#############################################################################
def getPosKmerFeature(seqsFilename, k, seqs=None, alphabet=None,
                   kmersDict=None, kmersList=None, dir=None):

  posKmerFeatureFilename="%s.%dnt.features.npz" % (seqsFilename, k)
  if dir is not None:
    posKmerFeatureFilename = dir + "/" + os.path.basename(posKmerFeatureFilename)

  if not os.path.isfile(posKmerFeatureFilename):
    assert seqs is not None
    posKmerFeatureArray = buildPosKmerFeature(seqs, k, alphabet=alphabet,
                                        kmersDict=kmersDict, kmersList=kmersList,
                                        outputfilename=posKmerFeatureFilename)
  else:
    posKmerFeatureArray = loadKmerFeature(posKmerFeatureFilename)

  return posKmerFeatureArray

#############################################################################
def getShapeFeature(seqsFilename, shape, seqs=None):

  shapeDict = calculateDNAShape(seqsFilename, seqs=seqs)
  if shape in shapeTypes:
    shapeFeatureArray = shapeDict[shape]
  elif shape == "shape":
    shapeFeatureArray = np.concatenate([shapeDict[shapeType] for shapeType in shapeTypes], axis=1)

  return shapeFeatureArray

#############################################################################
def getKmerShapeFeature(seqsFilename, k, shape, seqs=None, alphabet=None,
                   uniqKmersDict=None, uniqKmersList=None,
                   kmersDict=None, kmersList=None,
                   dir=None):

  kmerShapeFeatureFilename="%s.%dmer.%s.features.npz" % (seqsFilename, k, shape)
  if dir is not None:
    kmerShapeFeatureFilename = dir + "/" + os.path.basename(kmerShapeFeatureFilename)

  if not os.path.isfile(kmerShapeFeatureFilename):
    assert seqs is not None
    kmerShapeFeatureArray = buildKmerShapeFeature(seqs, k, alphabet=alphabet,
                              uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                              kmersDict=kmersDict, kmersList=kmersList,
                              kmerShapeDict=None,
                              outputfilename=kmerShapeFeatureFilename)
  else:
    kmerShapeFeatureArray = loadKmerShapeFeature(kmerShapeFeatureFilename)

#  print "getKmerShapeFeature"
#  print kmerShapeFeatureArray.shape
  return kmerShapeFeatureArray

#############################################################################
def getDimismatchDist(k, alphabet=None, dir=None):

  dimismatchDistFilename = "%dmer.dm.dist.npy" % k
  if dir is not None:
    dimismatchDistFilename = dir + "/" + dimismatchDistFilename

  if not os.path.isfile(dimismatchDistFilename):
    kmersDict,kmersList = getKmer(k,dir=dir)
    uniqKmersDict,uniqKmersList = getUniqKmer(k, kmersDict=kmersDict, kmersList=kmersList,dir=dir)

    if k <= 2:
      dimismatchDist = buildDimismatchDist(k, alphabet=alphabet,
                                           uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                           kmersDict=kmersDict, kmersList=kmersList,
                                           outputfilename=dimismatchDistFilename)
    else:
      pDimismatchDist = getDimismatchDist(k-1,dir=dir)
      pKmersDict,pKmersList = getKmer(k-1,dir=dir)
      pUniqKmersDict,pUniqKmersList = getUniqKmer(k-1, kmersDict=pKmersDict, kmersList=pKmersList,dir=dir)
      dimismatchDist = buildDimismatchDist(k, alphabet=alphabet,
                                           uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                           kmersDict=kmersDict, kmersList=kmersList,
                                           pDimismatchDist=pDimismatchDist,
                                           pUniqKmersDict=pUniqKmersDict, pUniqKmersList=pUniqKmersList,
                                           pKmersDict=pKmersDict, pKmersList=pKmersList,
                                           outputfilename=dimismatchDistFilename)
  else:
    dimismatchDist = loadDimismatchDist(dimismatchDistFilename)

  return dimismatchDist

#############################################################################
def getDimismatchTable(k, m, alphabet=None,
                       uniqKmersDict=None, uniqKmersList=None, kmersDict=None, kmersList=None,
                       dimismatchDist=None,
                       nDimismatchTable=None,
                       dir=None):

  dimismatchTableFilename = "%dmer.%dm.npz" % (k,m)
  if dir is not None:
    dimismatchTableFilename = dir + "/" + dimismatchTableFilename

  if not os.path.isfile(dimismatchTableFilename):
    dimismatchTable = buildDimismatchTable(k, m, alphabet=alphabet,
                                           uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                           kmersDict=kmersDict, kmersList=kmersList,
                                           dimismatchDist=dimismatchDist,
                                           nDimismatchTable=nDimismatchTable,
                                           outputfilename=dimismatchTableFilename)
  else:
    dimismatchTable = loadDimismatchTable(dimismatchTableFilename)

  return dimismatchTable

#############################################################################
def getDimismatchFeature(seqsFilename, seqs, k, m, alphabet=None,
                         uniqKmersDict=None, uniqKmersList=None, kmersDict=None, kmersList=None,
                         indir=None, outdir=None):

  dimismatchFeatureFilename= "%s.%dmer.%dm.features.npz" % (seqsFilename,k,m)
  if outdir is not None:
    dimismatchFeatureFilename = outdir + "/" + os.path.basename(dimismatchFeatureFilename)

  if not os.path.isfile(dimismatchFeatureFilename):
    dimismatchTable = getDimismatchTable(k=k, m=m, alphabet=alphabet,
                                         uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                         kmersDict=kmersDict, kmersList=kmersList, dir=indir)

    dimismatchFeatureArray = buildDimismatchFeature(seqs, k, m, alphabet=alphabet,
                                           uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                           kmersDict=kmersDict, kmersList=kmersList,
                                           dimismatchTable=dimismatchTable,
                                           outputfilename=dimismatchFeatureFilename)
  else:
    dimismatchFeatureArray = loadDimismatchFeature(dimismatchFeatureFilename)

  return dimismatchFeatureArray

#############################################################################
def getDimismatchShapeFeature(seqsFilename, seqs, k, m, shape, alphabet=None,
                              uniqKmersDict=None, uniqKmersList=None, kmersDict=None, kmersList=None,
                              indir=None, outdir=None):

#  kmerfilename = "%dmer.seq.gz" % k
#  if indir is not None:
#    kmerfilename = indir + "/" + kmerfilename

  dimismatchShapeFeatureFilename = "%s.%dmer.%dm.shape.features.npz" % (seqsFilename, k, m)

  if outdir is not None:
    dimismatchShapeFeatureFilename = outdir + "/" + os.path.basename(dimismatchShapeFeatureFilename)

  if not os.path.isfile(dimismatchShapeFeatureFilename):

    dimismatchDNAShapeFilehead = "%dmer.%dm"  % (k,m)
    if indir is not None:
      dimismatchDNAShapeFilehead = indir + "/" + dimismatchDNAShapeFilehead

    dimismatchTable = getDimismatchTable(k=k, m=m, alphabet=alphabet,
                                         uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                         kmersDict=kmersDict, kmersList=kmersList, dir=indir)

    if not os.path.isfile(dimismatchDNAShapeFilehead+".MGW.npz"):
    #  kmerShapeDict = calculateKmerDNAShape(k, alphabet=alphabet, kmersDict=kmersDict, kmersList=kmersList)
      kmerShapeDict = calculateKmerDNAShape(k=k+4, alphabet=alphabet, flanking=True)

      dimismatchShapeDict = buildDimismatchDNAShapeTable(k=k, m=m, dimismatchTable=dimismatchTable, kmerShapeDict=kmerShapeDict,
                                               uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList, kmersDict=kmersDict, kmersList=kmersList,
                                               outputfilename=dimismatchDNAShapeFilehead)
    else:
      print dimismatchDNAShapeFilehead
      dimismatchShapeDict = loadDimismatchDNAShapeTable(dimismatchDNAShapeFilehead)

    dimismatchShapeFeatureArray = buildDimismatchShapeFeature(seqs, k=k, m=m,
                                                        uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList, kmersDict=kmersDict, kmersList=kmersList,
                                                        dimismatchTable=dimismatchTable, dimismatchShapeDict=dimismatchShapeDict,
                                                        outputfilename=dimismatchShapeFeatureFilename)
  else:
    dimismatchShapeFeatureArray = loadDimismatchShapeFeature(dimismatchShapeFeatureFilename)

  if shape in shapeTypes:
    shapeIndex = shapeTypes.index(shape)
    return dimismatchShapeFeatureArray[:,len(uniqKmersList)*(shapeIndex+1):len(uniqKmersList)*(shapeIndex+2)]
  elif shape == "shape":
    return dimismatchShapeFeatureArray[:,len(uniqKmersList):]
