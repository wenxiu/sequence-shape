# AUTHOR: Wenxiu Ma
# MODIFIED DATA: 18 May 2015

from utils import *

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import sys
import numpy as np
from scipy import sparse
from scipy.sparse import csr_matrix

from sklearn import svm
from sklearn import preprocessing
from sklearn import cross_validation
from sklearn import grid_search
from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import r2_score
from sklearn.base import BaseEstimator,TransformerMixin
#from sklearn.utils import check_arrays #sklearn/0.14.0
from sklearn.utils import check_array #sklearn/0.16.1
from sklearn.utils import warn_if_not_float
from sklearn.utils.validation import check_is_fitted

Cs = np.logspace(-3, 3, num=7, base=10)
#Cs = np.logspace(-3, 3, num=5, base=10)
#epsilons = np.logspace(-6, 0, num=7, base=2)
#epsilons = np.linspace(0, 1, num=11)
epsilons = np.array([0,0.001,0.01,0.1,0.2,0.5,1.0])

#############################################################################
# get column-wise min or max for CSR sparse matrix

def min_sparse(X):
  if sparse.issparse(X):
    if len(X.data) == 0:
      return 0
    m = X.data.min()
    return m if X.nnz == X.shape[0]*X.shape[1] else min(m, 0)
#    return min(m,0.)
  else:
    return np.min(X)

def col_min_sparse(X):
  if sparse.issparse(X):
    out = np.zeros(X.shape[1], dtype=float)
    for i in xrange(X.shape[1]):
      out[i] = min_sparse(X.getcol(i))
    return out
  else:
    return np.min(X, axis=0)

def max_sparse(X):
  if sparse.issparse(X):
    if len(X.data) == 0:
      return 0
    m = X.data.max()
    return m if X.nnz == X.shape[0]*X.shape[1] else max(m, 0)
#    return max(m,0.)
  else:
    return np.max(X)

def col_max_sparse(X):
  if sparse.issparse(X):
    out = np.zeros(X.shape[1], dtype=float)
    for i in xrange(X.shape[1]):
      out[i] = max_sparse(X.getcol(i))
    return out
  else:
    return np.max(X, axis=0)

#############################################################################
# modify sklearn/preprocessing.MinMaxScaler to accept csr sparse matrix
# note that the code is only good for sklearn/0.14.0

class MinMaxScaler(BaseEstimator, TransformerMixin):
  """Standardizes features by scaling each feature to a given range.
  This estimator scales and translates each feature individually such
  that it is in the given range on the training set, i.e. between
  zero and one.
  The standardization is given by::
    X_std = (X - X.min(axis=0)) / (X.max(axis=0) - X.min(axis=0))
    X_scaled = X_std * (max - min) + min
  where min, max = feature_range.
  This standardization is often used as an alternative to zero mean,
  unit variance scaling.
  Parameters
  ----------
  feature_range: tuple (min, max), default=(0, 1)
    Desired range of transformed data.
  copy : boolean, optional, default True
    If False, try to avoid a copy and do inplace scaling instead.
    This is not guaranteed to always work inplace; e.g. if the data is
    not a NumPy array or scipy.sparse CSR matrix, a copy may still be
    returned.
  Attributes
  ----------
  min_ : ndarray, shape (n_features,)
    Per feature adjustment for minimum.
  scale_ : ndarray, shape (n_features,)
    Per feature relative scaling of the data.
  """

  def __init__(self, feature_range=(0, 1), copy=True):
    self.feature_range = feature_range
    self.copy = copy

  def fit(self, X, y=None):
    """Compute the minimum and maximum to be used for later scaling.
    Parameters
    ----------
    X : array-like, or CSR matrix, shape [n_samples, n_features]
      The data used to compute the per-feature minimum and maximum
      used for later scaling along the features axis.
    """
    X = check_array(X, accept_sparse='csr', copy=self.copy, ensure_2d=False)
#    X = check_arrays(X, copy=self.copy, sparse_format="csr")[0] #sklearn/0.14.0
    warn_if_not_float(X, estimator=self)
    feature_range = self.feature_range
    if feature_range[0] >= feature_range[1]:
      raise ValueError("Minimum of desired feature range must be smaller"
              " than maximum. Got %s." % str(feature_range))
    data_min = col_min_sparse(X)
    data_range = col_max_sparse(X) - data_min
    # Do not scale constant features
    if isinstance(data_range, np.ndarray):
      data_range[data_range == 0.0] = 1.0
    elif data_range == 0.:
      data_range = 1.
    self.scale_ = (feature_range[1] - feature_range[0]) / data_range
#    print self.scale_
    self.min_ = feature_range[0] - data_min * self.scale_
#    print self.min_
    self.data_range = data_range
    self.data_min = data_min

    return self

  def transform(self, X):
    """Scaling features of X according to feature_range.
    Parameters
    ----------
    X : array-like, or CSR matrix, with shape [n_samples, n_features]
      Input data that will be transformed.
    """
    check_is_fitted(self, 'scale_')
    X = check_array(X, accept_sparse='csr', copy=self.copy, ensure_2d=False)
#    X = check_arrays(X, copy=self.copy, sparse_format="csr")[0] #sklearn/0.14.0
    if sparse.issparse(X):
      X = X.multiply(csr_matrix(self.scale_))
      X = X + sparse.vstack([csr_matrix(self.min_) for _ in xrange(X.shape[0])])
    else:
      X *= self.scale_
      X += self.min_

    return X

  def inverse_transform(self, X):
    """Undo the scaling of X according to feature_range.
    Parameters
    ----------
    X : array-like with shape [n_samples, n_features]
      Input data that will be transformed.
    """
    check_is_fitted(self, 'scale_')
    X = check_array(X, accept_sparse='csr', copy=self.copy, ensure_2d=False)
#    X = check_arrays(X, copy=self.copy, sparse_format="csr")[0] #sklearn/0.14.0
    if sparse.issparse(X):
      X = X - sparse.vstack([csr_matrix(self.min_) for _ in xrange(X.shape[0])])
      X = X.multiply(csr_matrix(1.0/self.scale_))
    else:
      X -= self.min_
      X /= self.scale_

    return X

#############################################################################
#  nested cross-validation

def svm_nested_cv(x, y, k_fold=3):

  print x.dtype
  # external cv
  sys.stderr.write("samples=%d\tfeatures=%d\n" % x.shape)
  feature_num = x.shape[1]
  skf = cross_validation.StratifiedKFold(y, n_folds=k_fold)
  finalscores = []
  for train_index, test_index in skf:
    sys.stderr.write("\n##### External fold %d #####\n" % (len(finalscores)+1) )
#    print len(train_index), len(test_index)
    x_train, x_test = x[train_index],x[test_index]
    y_train, y_test = y[train_index],y[test_index]
#    print x_train[0,52]
#    np.savetxt("test_train",x_train.toarray()[:,52])

    # scale the data - dense matrix
#    min_max_scaler = preprocessing.MinMaxScaler()
#    x_train_scaled = min_max_scaler.fit_transform(x_train.toarray())
#    x_test_scaled = min_max_scaler.transform(x_test.toarray())
#    print min_max_scaler.scale_
#    print min_max_scaler.min_
#    print x_train_scaled_d
#    print x_train_scaled_d.shape, x_train_scaled_d.dtype
#    print x_train_scaled_d[0,52]
#    print min_max_scaler.scale_[52]
#    print min_max_scaler.min_[52]
#    print min_max_scaler.data_range[52]
#    print min_max_scaler.data_min[52]

    # scale the data - dense matrix to sparse matrix
#    min_max_scaler = preprocessing.MinMaxScaler()
#    x_train_scaled = csr_matrix(min_max_scaler.fit_transform(x_train.toarray()))
#    x_test_scaled = csr_matrix(min_max_scaler.transform(x_test.toarray()))

    # scale the data - sparse matrix
    min_max_scaler = MinMaxScaler()
    x_train_scaled = min_max_scaler.fit_transform(x_train)
    x_test_scaled = min_max_scaler.transform(x_test)

#    print x_train_scaled_d == x_train_scaled
#    print np.all(x_train_scaled_d == x_train_scaled)
#    print np.where(x_train_scaled_d != x_train_scaled)
#    print x_train_scaled_d[x_train_scaled_d != x_train_scaled]
#    print x_train_scaled.toarray()[x_train_scaled_d != x_train_scaled]

    # internal cv and grid-search
    clf = grid_search.GridSearchCV(estimator=svm.SVC(C=1.0, kernel="linear"),
#                                   param_grid=[{'kernel': ['linear'], 'C': Cs}],
                                   param_grid=dict(C=Cs),
                                   scoring='roc_auc',
                                   cv=k_fold,
                                   n_jobs=1)
#    print clf.estimator
    clf.fit(x_train_scaled, y_train)
    sys.stderr.write("Best parameters set found on development set:\n")
    sys.stderr.write("%r\n" % clf.best_estimator_)
    sys.stderr.write("Best score set found on development set: %r\n" % clf.best_score_)
    sys.stderr.write("Grid scores on development set:\n")
    for params, mean_score, scores in clf.grid_scores_:
      sys.stderr.write("%0.3f (+/-%0.03f) for %r\n" % (mean_score, scores.std() / 2, params))

    sys.stderr.write("Detailed classification report:\n")
    y_true, y_pred = y_test, clf.predict(x_test_scaled)
    sys.stderr.write("%s\n" % classification_report(y_true, y_pred))
#    print >> sys.stderr classification_report(y_true, y_pred)
#    print clf.scorer_
#    score=clf.score(x_test_scaled, y_test) # score function only return accuracy for binary classification
#    print score
    dist_ = clf.decision_function(x_test_scaled)
    # Compute ROC curve and area the curve
    fpr, tpr, thresholds = roc_curve(y_test, dist_)
    roc_auc = auc(fpr, tpr)
    sys.stderr.write("Test on evaluation set using best parameters set: AUC = %f\n" % roc_auc)
    finalscores.append(roc_auc)
    sys.stdout.flush()

  sys.stderr.write("Nested cross-validation completed.\n")
  sys.stderr.write("AUC = %0.3f (+/-%0.03f)\n" % (np.mean(finalscores), np.std(finalscores)/2))
  sys.stderr.flush()

  return feature_num, finalscores


#############################################################################
# nested cross-validation with feature selection
# in each inner cross-validation we first select top features and then select model parameters associated with the top set of features

def svm_fc_nested_cv(x, y, k_fold=3, max_feature_num=4000, feature_names=None):

  feature_num = x.shape[1]
  if max_feature_num >= feature_num:
#    sys.exit(1)
    return svm_nested_cv(x,y,k_fold=k_fold)

  if feature_names is not None:
    assert len(feature_names) == feature_num, "parameter feature_names is of length %d NOT %d" % (len(feature_names), feature_num)

  # external cv
  sys.stderr.write("samples=%d\tfeatures=%d\n" % x.shape)
  skf = cross_validation.StratifiedKFold(y, n_folds=k_fold)
  finalscores = []
  for train_index, test_index in skf:
    sys.stderr.write("\n##### External fold %d #####\n" % (len(finalscores)+1) )
#    print len(train_index), len(test_index)
    x_train, x_test = x[train_index],x[test_index]
    y_train, y_test = y[train_index],y[test_index]

    # scale the data
    min_max_scaler = MinMaxScaler()
    x_train_scaled = min_max_scaler.fit_transform(x_train)
    x_test_scaled = min_max_scaler.transform(x_test)

    # internal cv and grid-search for both feature selections first
    skf_inner = cross_validation.StratifiedKFold(y_train, n_folds=k_fold)
    feature_scores=np.zeros(shape=feature_num) # row is feature

    clf = grid_search.GridSearchCV(estimator=svm.SVC(C=1.0, kernel="linear"),
#                                   param_grid=[{'kernel': ['linear'], 'C': Cs}],
                                   param_grid=dict(C=Cs),
                                   scoring='roc_auc',
                                   cv=skf_inner,
                                   n_jobs=1)

    for feature_index in xrange(feature_num):
#      clf.fit(x_train_scaled[:,(feature_index,)], y_train)
#      sys.stderr.write("Best parameters set found on development set feature %d:\n" % feature_index)
#      sys.stderr.write("%r\n" % clf.best_estimator_)
#      sys.stderr.write("Best score set found on development set feature %d: %r\n" % (feature_index, clf.best_score_))
#      sys.stderr.write("Grid scores on development set:\n")
#      for params, mean_score, scores in clf.grid_scores_:
#        sys.stderr.write("%0.3f (+/-%0.03f) for %r\n" % (mean_score, scores.std() / 2, params))
#      feature_scores[feature_index] = clf.best_score_

#      auc_scores = np.zeros(shape=k_fold)
#      skf_inner_index = 0
#      for train_inner_index, test_inner_index in skf_inner:
#        sys.stderr.write("\n##### Feature %d - Inner fold %d #####\n" % (feature_index, skf_inner_index))
#        x_train_inner, x_test_inner = x_train_scaled[train_inner_index],x_train_scaled[test_inner_index]
#        y_train_inner, y_test_inner = y_train[train_inner_index],y_train[test_inner_index]
#        fpr, tpr, thresholds = roc_curve(y_train_inner, x_train_inner[:,feature_index])
#        roc_auc = auc(fpr, tpr)
#        auc_scores[skf_inner_index] = roc_auc
#        skf_inner_index += 1
#        sys.stderr.write("score: %f:\n" % roc_auc)
#      feature_scores[feature_index] = np.max(auc_scores)

      fpr, tpr, thresholds = roc_curve(y_train, np.reshape(x_train_scaled[:,feature_index].toarray(),x_train_scaled.shape[0]))
      roc_auc = auc(fpr, tpr)
      feature_scores[feature_index] = roc_auc
#      sys.stderr.write("Best score set found on development set feature %d: %f\n" % (feature_index, feature_scores[feature_index]))

    # select best features
    feature_selected=np.argsort(feature_scores)[-max_feature_num:]
    print feature_selected
    if feature_names is not None:
      print np.array(feature_names)[feature_selected]
    print feature_scores[np.array(feature_selected)]
    clf.fit(x_train_scaled[:,feature_selected], y_train)
    sys.stderr.write("Best parameters set found on development set: \n")
    sys.stderr.write("%r\n" % clf.best_estimator_)
    sys.stderr.write("Best score set found on development set feature: %r\n" % clf.best_score_)
    sys.stderr.write("Grid scores on development set:\n")
    for params, mean_score, scores in clf.grid_scores_:
      sys.stderr.write("%0.3f (+/-%0.03f) for %r\n" % (mean_score, scores.std() / 2, params))

    sys.stderr.write("Detailed classification report:\n")
    y_true, y_pred = y_test, clf.predict(x_test_scaled[:,feature_selected])
    sys.stderr.write("%s\n" % classification_report(y_true, y_pred))
    # Compute ROC curve and area the curve
    r2 = r2_score(y_true, y_pred)
    sys.stderr.write("Test on evaluation set using best parameters set: R2 = %f\n" % r2)
    finalscores.append(r2)
    sys.stdout.flush()

  sys.stderr.write("Nested cross-validation completed.\n")
  sys.stderr.write("R2 = %0.3f (+/-%0.03f)\n" % (np.mean(finalscores), np.std(finalscores)/2))
  sys.stderr.flush()

  return max_feature_num, finalscores

#############################################################################
#  nested cross-validation

def svr_nested_cv(x, y, k_fold=3):

  print x.dtype
  # external cv
  sys.stderr.write("samples=%d\tfeatures=%d\n" % x.shape)
  feature_num = x.shape[1]
  skf = cross_validation.KFold(len(y), n_folds=k_fold, shuffle=True)
  finalscores = []
  for train_index, test_index in skf:
    printTime()
    sys.stderr.write("\n##### External fold %d #####\n" % (len(finalscores)+1) )
#    print len(train_index), len(test_index)
    x_train, x_test = x[train_index],x[test_index]
    y_train, y_test = y[train_index],y[test_index]
#    print y_train, y_test

    # scale the data - dense matrix to sparse matrix
#    min_max_scaler = preprocessing.MinMaxScaler()
#    x_train_scaled = csr_matrix(min_max_scaler.fit_transform(x_train.toarray()))
#    x_test_scaled = csr_matrix(min_max_scaler.transform(x_test.toarray()))
#    np.save("test.x.np", x_train_scaled.toarray())
#    np.savetxt("test.y",y_train)

    # scale the data - sparse matrix
    min_max_scaler = MinMaxScaler()
    x_train_scaled = min_max_scaler.fit_transform(x_train)
    x_test_scaled = min_max_scaler.transform(x_test)

    # internal cv and grid-search
    skf_inner = cross_validation.KFold(len(y_train), n_folds=k_fold, shuffle=True)
    clf = grid_search.GridSearchCV(estimator=svm.SVR(C=1.0, kernel="linear"),
#                                   param_grid=[{'kernel': ['linear'], 'C': Cs}],
                                   param_grid=dict(C=Cs,epsilon=epsilons),
                                   scoring='r2',
#                                   cv=k_fold,
                                   cv=skf_inner,
                                   n_jobs=1)
#    print clf.estimator
    if sparse.issparse(x_train_scaled):
      clf.fit(x_train_scaled.toarray(), y_train)
    else:
      clf.fit(x_train_scaled, y_train)
    sys.stderr.write("Best parameters set found on development set:\n")
    sys.stderr.write("%r\n" % clf.best_estimator_)
    sys.stderr.write("Best score set found on development set: %r\n" % clf.best_score_)
    sys.stderr.write("Grid scores on development set:\n")
    for params, mean_score, scores in clf.grid_scores_:
      sys.stderr.write("%0.3f (+/-%0.03f) for %r\n" % (mean_score, scores.std() / 2, params))
    printTime("Complete fitting")

#    sys.stderr.write("Detailed classification report:\n")
    if sparse.issparse(x_test_scaled):
      y_true, y_pred = y_test, clf.predict(x_test_scaled.toarray())
    else:
      y_true, y_pred = y_test, clf.predict(x_test_scaled)

#    sys.stderr.write("%s\n" % classification_report(y_true, y_pred))
#    print >> sys.stderr classification_report(y_true, y_pred)
#    print clf.scorer_
#    score=clf.score(x_test_scaled, y_test) # score function only return accuracy for binary classification
#    print score

    # r2 score
    r2 = r2_score(y_true, y_pred)
    sys.stderr.write("Test on evaluation set using best parameters set: R2 = %f\n" % r2)
    finalscores.append(r2)
    sys.stdout.flush()
    printTime("Complete testing")

  sys.stderr.write("Nested cross-validation completed.\n")
  sys.stderr.write("R2 = %0.3f (+/-%0.03f)\n" % (np.mean(finalscores), np.std(finalscores)/2))
  sys.stderr.flush()

  return feature_num, finalscores

#############################################################################
# nested cross-validation with feature selection
# in each inner cross-validation we first select top features and then select model parameters associated with the top set of features

def svr_fc_nested_cv(x, y, k_fold=3, max_feature_num=1000, feature_names=None,
                     outputfilehead=None, k_fold_ind=None):
  # k_fold_ind = 0...k_fold-1, run only one outter CV

  print x.dtype

  feature_num = x.shape[1]
  if max_feature_num >= feature_num:
#    return svr_nested_cv(x,y,k_fold=k_fold)
    featureSelection = False
  else:
    featureSelection = True

  if feature_names is not None:
    assert len(feature_names) == feature_num, "parameter feature_names is of length %d NOT %d" % (len(feature_names), feature_num)

  printTime("Start CV")
  # external cv
  sys.stderr.write("samples=%d\tfeatures=%d\n" % x.shape)
  skf = cross_validation.KFold(len(y), n_folds=k_fold, shuffle=True, random_state=139)
  skflist = list(skf)
  finalscores = []
#  for train_index, test_index in skf:
  for skf_ind in xrange(len(skf)):
    sys.stderr.write("skf_ind=%d\n" % skf_ind)
    if k_fold_ind is not None and skf_ind != k_fold_ind-1:
      continue
    sys.stderr.write("\n##### External fold %d #####\n" % (skf_ind+1) )
    printTime()
#    print len(train_index), len(test_index)
#    print train_index, test_index

    r2filename = "%s.grid%d.r2" % (outputfilehead, skf_ind+1)
    if os.path.exists(r2filename):
      r2file = open(r2filename,"r")
      r2 = float(r2file.readline())
      r2file.close()

    else:
      train_index, test_index = skflist[skf_ind]
      x_train, x_test = x[train_index],x[test_index]
      y_train, y_test = y[train_index],y[test_index]

      # scale the data
      min_max_scaler = MinMaxScaler()
      x_train_scaled = min_max_scaler.fit_transform(x_train)
      x_test_scaled = min_max_scaler.transform(x_test)

      # internal cv and grid-search for both feature selections first
      skf_inner = cross_validation.KFold(len(y_train), n_folds=k_fold, shuffle=True)

      clf = grid_search.GridSearchCV(estimator=svm.SVR(C=1.0, kernel="linear"),
  #                                   param_grid=[{'kernel': ['linear'], 'C': Cs}],
                                     param_grid=dict(C=Cs,epsilon=epsilons),
                                     scoring='r2',
                                     cv=skf_inner,
  #                                   cv=k_fold,
                                     n_jobs=1)

      if not featureSelection:
  #      print clf.estimator
        if sparse.issparse(x_train_scaled):
          clf.fit(x_train_scaled.toarray(), y_train)
        else:
          clf.fit(x_train_scaled, y_train)

      else:
        # feature selection - optimized code
        printTime("Start feature selection")
        feature_scores=np.zeros(shape=feature_num) # row is feature
        if sparse.issparse(x_train_scaled):
          x_train_scaled = x_train_scaled.tocsc()
          x_train_scaled_nonzero_index = np.nonzero(np.diff(x_train_scaled.indptr))[0]
          for nonzero_feature_index in x_train_scaled_nonzero_index:
            x_train_feature = x_train_scaled[:,nonzero_feature_index].toarray()[:,0]
  #          print x_train_feature
            cc = np.corrcoef(x_train_feature,y_train)
            feature_scores[nonzero_feature_index]=cc[0,1]*cc[0,1]
        else: # dense x_train_scaled matrix
          for feature_index in xrange(feature_num):
            x_train_feature = x_train_scaled[:,feature_index]
            cc = np.corrcoef(x_train_feature,y_train)
            if np.isnan(cc[0,1]):
              feature_scores[feature_index] = 0.0
            else:
              feature_scores[feature_index]=cc[0,1]*cc[0,1]

        # select best features
        feature_selected=np.argsort(feature_scores)[-max_feature_num:]
        print feature_selected
        if feature_names is not None:
          print np.array(feature_names)[feature_selected]
        print feature_scores[np.array(feature_selected)]
        printTime("Complete feature selection")

  #      print (feature_scores == feature_scores_p).all()
  #      sys.stdout.flush()

        if sparse.issparse(x_train_scaled):
  #        print x_train_scaled
          x_train_selected = x_train_scaled[:,feature_selected].toarray()
        else:
          x_train_selected = x_train_scaled[:,feature_selected]
        printTime("Extracted seleted features")

        clf.fit(x_train_selected, y_train)
        printTime("Complete fitting")

      sys.stderr.write("Best parameters set found on development set: \n")
      sys.stderr.write("%r\n" % clf.best_estimator_)
      sys.stderr.write("Best score set found on development set feature: %r\n" % clf.best_score_)
      sys.stderr.write("Grid scores on development set:\n")
      for params, mean_score, scores in clf.grid_scores_:
        sys.stderr.write("%0.3f (+/-%0.03f) for %r\n" % (mean_score, scores.std() / 2, params))

      # plot the scores of the grid
      # grid_scores_ contains parameter settings and scores
      # We extract just the scores
      gridscores = [xscore[1] for xscore in clf.grid_scores_]
      gridscores = np.array(gridscores).reshape(len(Cs), len(epsilons))

      # Draw heatmap of the validation accuracy as a function of gamma and C
      # The score are encoded as colors with the hot colormap which varies from dark
      # red to bright yellow. As the most interesting scores are all located in the
      # 0.92 to 0.97 range we use a custom normalizer to set the mid-point to 0.92 so
      # as to make it easier to visualize the small variations of score values in the
      # interesting range while not brutally collapsing all the low score values to
      # the same color.

      fig = plt.figure(figsize=(8, 6))
      plt.subplots_adjust(left=.2, right=0.95, bottom=0.15, top=0.95)
  #    plt.imshow(gridscores, interpolation='nearest', cmap=plt.cm.hot,
  #               norm=MidpointNormalize(vmin=0.2, midpoint=0.92))
      plt.imshow(gridscores, interpolation='nearest', cmap=plt.cm.hot)
      plt.xlabel('epsilon')
      plt.ylabel('C')
      plt.colorbar()
      plt.xticks(np.arange(len(epsilons)), epsilons, rotation=45)
      plt.yticks(np.arange(len(Cs)), Cs)
      plt.title('R2 scores')
      plt.show()
      plt.draw()
      pngfilename = "%s.grid%d.heatmap.png" % (outputfilehead, skf_ind+1)
      fig.savefig(pngfilename)

      if not featureSelection:
        if sparse.issparse(x_test_scaled):
          y_true, y_pred = y_test, clf.predict(x_test_scaled.toarray())
        else:
          y_true, y_pred = y_test, clf.predict(x_test_scaled)
      else:
        if sparse.issparse(x_test_scaled):
          y_true, y_pred = y_test, clf.predict(x_test_scaled.tocsc()[:,feature_selected].toarray())
        else:
          y_true, y_pred = y_test, clf.predict(x_test_scaled[:,feature_selected])

      # Compute r2 score
      r2 = r2_score(y_true, y_pred)
      sys.stderr.write("Test on evaluation set using best parameters set: R2 = %f\n" % r2)

      # scatterplot of y_true vs y_pred
      fig = plt.figure(figsize=(6, 6))
      plt.scatter(y_true,y_pred)
      plt.xlabel('y_true')
      plt.ylabel('y_pred')
  #    plt.xticks(np.arange(len(epsilons)), epsilons, rotation=45)
  #    plt.yticks(np.arange(len(Cs)), Cs)
      plt.title('r2=%f' % r2)
      plt.show()
      plt.draw()
      pngfilename = "%s.grid%d.scatterplot.png" % (outputfilehead, skf_ind+1)
      fig.savefig(pngfilename)

      r2file = open(r2filename,"w")
#      r2 = float(r2file.readline())
      r2file.write("%f\n" % r2)
      r2file.close()

      sys.stdout.flush()
      printTime("Complete testing")

    finalscores.append(r2)

  sys.stderr.write("Nested cross-validation completed.\n")
  sys.stderr.write("R2 = %0.3f (+/-%0.03f)\n" % (np.mean(finalscores), np.std(finalscores)/2))
  sys.stderr.flush()

  return feature_num, finalscores
