'''This library contains functions to obtain, read, write dna shape values'''

import os
import sys
import numpy as np
import gzip
import string
from scipy.sparse import csr_matrix, lil_matrix
import fileinput # get file line number

import utils

shapeTypes=("MGW", "Roll", "ProT", "HelT")
nShape=len(shapeTypes)

complement = string.maketrans('ATCGN', 'TAGCN')
dinucleotide = {
  'AA':'A',
  'AC':'B',
  'AG':'C',
  'AT':'D',
  'CA':'E',
  'CC':'F',
  'CG':'G',
  'CT':'H',
  'GA':'I',
  'GC':'J',
  'GG':'K',
  'GT':'L',
  'TA':'M',
  'TC':'N',
  'TG':'O',
  'TT':'P',
  'AN':'Q',
  'CN':'R',
  'GN':'S',
  'TN':'T',
  'NA':'U',
  'NC':'V',
  'NG':'W',
  'NT':'X',
  'NN':'Y'
}

#############################################################################
# Used to take the complement of a string.

def reverseComplement(sequence):
  return sequence.upper().translate(complement)[::-1]

#############################################################################
# calculate dimismatch hamming distance

def diHamming(seq1, seq2):
  assert len(seq1) == len(seq2), "Error! %s and %s are not of the same length!" % (seq1,seq2)
  seq1U = seq1.upper()
  seq2U = seq2.upper()
  prePos = -1
  distance = 0
  for pos in xrange(len(seq1U)):
    if seq1U[pos] != seq2U[pos]:
      if prePos+1 == pos:
        distance += 1
      else:
        distance += 2
      if pos == len(seq1U)-1:
        distance -= 1
      prePos=pos
  return distance


#############################################################################
# save and load sparse csr_matrix

def save_sparse_csr(filename,array):
  np.savez(filename, data = array.data, indices=array.indices,
           indptr =array.indptr, shape=array.shape)

def load_sparse_csr(filename):
  loader = np.load(filename)
  return csr_matrix((loader['data'], loader['indices'], loader['indptr']),
                    shape = loader['shape'])

#############################################################################
# generate all possile k-mers and store in a list

def generateKmer(k, alphabet=None, outputfilename=None):
  assert k>0, "paremeter k=%d needs to be positive" % k
  if alphabet is None:
    alphabet="ACGT"
  alphabet_len=len(alphabet)
#  total=pow(alphabet_len,k)
  kmersList=[""]
  kmersDict={}
  for k_ind in xrange(k):
    # pop out strings of len (k_ind-1)
    poptotal=pow(alphabet_len, k_ind)
    kmersNum=0
    for pop_ind in xrange(poptotal):
      oldseq=kmersList.pop(0)
      for char_ind in xrange(alphabet_len):
        newseq = oldseq+alphabet[char_ind]
        kmersList.append(newseq)
        if k_ind==k-1:
          kmersDict[newseq] = kmersNum
          kmersNum += 1

  # write kmer to gzipped file
  if outputfilename is not None:
    if outputfilename[-3:] == ".gz":
      output = gzip.open(outputfilename, "w")
    else:
      output = open(outputfilename, "w")
    for kmer in kmersList:
      output.write("%s\n" % kmer)
    output.close()

  sys.stderr.write("Generate %d %d-mers\n" % (len(kmersDict),k))
  return kmersDict,kmersList

#############################################################################
# load all possile k-mers and store in a list

def loadKmer(inputfilename, k=None):

  if inputfilename[-3:] == ".gz":
    inputfile = gzip.open(inputfilename)
  else:
    inputfile = open(inputfilename)

  kmersList = [line.strip() for line in inputfile]
  kmersDict={}
  for kmer_ind in xrange(len(kmersList)):
    kmersDict[kmersList[kmer_ind]] = kmer_ind

  if k is not None:
    assert k==len(kmersList[0]), "paremeter k=%d needs to be match sequences in inputfile." % k
  else:
    k=len(kmersList[0])

  sys.stderr.write("Read %d %d-mers\n" % (len(kmersDict),k))
  return kmersDict,kmersList

#############################################################################
# generate all unique kmersList and store in dictionary and List

def generateUniqKmer(k, alphabet=None, kmersDict=None, kmersList=None, outputfilename=None):
  assert k>0, "paremeter k=%d needs to be positive" % k
  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)

  uniqKmersDict = {} # key is kmer; values are index of seq and rev-comp-seq
  uniqKmersList = []
  for kmer_ind in xrange(len(kmersList)):
    kmer=kmersList[kmer_ind]
    kmerRevComp=reverseComplement(kmer)
#    sys.stderr.write("%s=>%s\n" % (kmer,kmerRevComp))
    if kmerRevComp in uniqKmersDict:
      uniqKmersDict[kmerRevComp].append(kmer_ind)
    else:
      uniqKmersDict[kmer]=[len(uniqKmersList),kmer_ind]
      uniqKmersList.append(kmer)

  # write kmer to gzipped file
  if outputfilename is not None:
    if outputfilename[-3:] == ".gz":
      output = gzip.open(outputfilename, "w")
    else:
      output = open(outputfilename, "w")
    for uniqKmer in uniqKmersList:
      if len(uniqKmersDict[uniqKmer]) == 2:
        output.write("%s %d %d\n" % (uniqKmer, uniqKmersDict[uniqKmer][0],uniqKmersDict[uniqKmer][1]))
      else:
        output.write("%s %s %d %d %d\n" % (uniqKmer,kmersList[uniqKmersDict[uniqKmer][2]],
                                        uniqKmersDict[uniqKmer][0], uniqKmersDict[uniqKmer][1], uniqKmersDict[uniqKmer][2]))
    output.close()

  sys.stderr.write("Generate %d uniq %d-mers\n" % (len(uniqKmersDict),k))
  return uniqKmersDict,uniqKmersList

#############################################################################
# read unique kmer file and re-install uniqKmersDict,uniqKmersList,kmersDict,kmersList

def loadUniqKmer(inputfilename, k=None):
  if inputfilename[-3:] == ".gz":
    inputfile = gzip.open(inputfilename)
  else:
    inputfile = open(inputfilename)

  uniqKmersDict = {} # key is kmer; values are index of seq and rev-comp-seq
  for line in inputfile:
    words = line.rstrip().split()
    if len(words) == 3:
      uniqKmer = words[0]
      uniqKmersDict[uniqKmer] = [int(words[1]),int(words[2])]
    elif len(words) == 5:
      uniqKmer = words[0]
      uniqKmerRev = words[1]
      uniqKmersDict[uniqKmer] = [int(words[2]),int(words[3]),int(words[4])]
    else:
      sys.stderr.write("Error! Wrong file format: %s" % line)

  if k is not None:
    assert k==len(uniqKmersDict.keys()[0]), "paremeter k=%d needs to be match sequences in inputfile." % k
  else:
    k=len(uniqKmer)

  sys.stderr.write("Read %d uniq %d-mers\n" % (len(uniqKmersDict),k))

  uniqKmersList = [None] * len(uniqKmersDict)
  for uniqKmer in uniqKmersDict:
    uniqKmersList[uniqKmersDict[uniqKmer][0]] = uniqKmer

  return uniqKmersDict,uniqKmersList


#############################################################################
# build k-mer kernel. rows are all training seqs, cols are all uniq k-mers

def buildKmerFeature(trainSeqs, k, alphabet=None,
                     uniqKmersDict=None, uniqKmersList=None,
                     kmersDict=None, kmersList=None, outputfilename=None):

  assert len(trainSeqs) > 0, "Error! Training sequence set can not be empty"
  assert k>0, "paremeter k=%d needs to be positive" % k

  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
  if uniqKmersDict is None or uniqKmersList is None:
    uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,kmersDict=kmersDict, kmersList=kmersList)

  nRows = len(trainSeqs)
  nCols = len(uniqKmersDict)

  kmerFeatureArray = lil_matrix((nRows, nCols), dtype=np.float)

  for seq_ind in xrange(nRows):
    for seq_kmer_start in xrange(len(trainSeqs[0])-k+1):
      seq_kmer=trainSeqs[seq_ind][seq_kmer_start:(seq_kmer_start+k)]
      if seq_kmer not in kmersDict:
        sys.stderr.write("Error! %s not a valid %d-mer!\n" % (seq_kmer,k))
        sys.exit(1)
      for j in xrange(nCols):
        jKmerFwd=uniqKmersList[j]
        jKmerInds=uniqKmersDict[jKmerFwd]
        if len(jKmerInds) == 2:
          jKmerRev = jKmerFwd
        else:
          jKmerRev=kmersList[jKmerInds[2]]
        if seq_kmer == jKmerFwd or seq_kmer == jKmerRev:
          kmerFeatureArray[seq_ind,j] += 1

  # write feature matrix to npy file
  if outputfilename is not None:
    if outputfilename[-4:] == ".npy":
      np.save(outputfilename, kmerFeatureArray.toarray())
    elif outputfilename[-4:] == ".npz":
      save_sparse_csr(outputfilename, kmerFeatureArray.tocsr())
    else:
      np.savetxt(outputfilename, kmerFeatureArray.toarray())

  return kmerFeatureArray.tocsr()

#############################################################################
# load k-mer kernel. rows are all training seqs, cols are all uniq k-mers

def loadKmerFeature(inputfilename):
  if inputfilename is not None:
    if inputfilename[-4:] == ".npy":
      kmerFeatureArray = np.load(inputfilename)
    elif inputfilename[-4:] == ".npz":
      kmerFeatureArray = load_sparse_csr(inputfilename)
    else:
      kmerFeatureArray = np.loadtxt(inputfilename)

  return kmerFeatureArray


#############################################################################
# calculate shape start index and total shape vector length for k-mer

def calculateShapeIndex(k):
  shapeIndexDict = {}
  index = 0
  for shapeType in shapeTypes:
    if shapeType == "MGW" or shapeType == "ProT":
      length = k
    else:
      length = (k+1)
    shapeIndexDict[shapeType] = (index,length)
    index += length
  return index,shapeIndexDict

#############################################################################
# build k-mer shape kernel. rows are all training seqs,
# cols are all uniq k-mers x average shape features for all instances of that k-mer 
#   * shape-length(k or k+1)

def buildKmerShapeFeature(trainSeqs, k, alphabet=None,
                     uniqKmersDict=None, uniqKmersList=None,
                     kmersDict=None, kmersList=None,
                     kmerShapeDict=None,
                     outputfilename=None):

  assert len(trainSeqs) > 0, "Error! Training sequence set can not be empty"
  assert k>0, "paremeter k=%d needs to be positive" % k

  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
  if uniqKmersDict is None or uniqKmersList is None:
    uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,kmersDict=kmersDict, kmersList=kmersList)

  if kmerShapeDict is None:
    kmerShapeDict=calculateKmerDNAShape(k=k+4, alphabet=alphabet, flanking=True)

  nRows = len(trainSeqs)
  uniqKmersNum = len(uniqKmersList)

  nCols, shapeIndexDict = calculateShapeIndex(k)
  print nCols, shapeIndexDict

#  nCols = uniqKmersNums*(4k+2) # k for MGW ProT and 

  kmerShapeFeatureArray = lil_matrix((nRows, nCols*uniqKmersNum), dtype=np.float)

  for seq_ind in xrange(nRows):
    trainSeq = trainSeqs[seq_ind]
    newTrainSeq = "NN"+trainSeq+"NN"
    for seq_kmer_start in xrange(len(trainSeq)-k+1):
      seq_kmer = trainSeq[seq_kmer_start:(seq_kmer_start+k)]
      if seq_kmer not in kmersDict:
        sys.stderr.write("Error! %s not a valid %d-mer!\n" % (seq_kmer,k))
        sys.exit(1)
      for j in xrange(uniqKmersNum):
        jKmerFwd=uniqKmersList[j]
        jKmerInds=uniqKmersDict[jKmerFwd]
        if len(jKmerInds) == 2:
          jKmerRev = jKmerFwd
        else:
          jKmerRev=kmersList[jKmerInds[2]]
        if seq_kmer == jKmerFwd or seq_kmer == jKmerRev:
          seq_kP4mer = newTrainSeq[seq_kmer_start:(seq_kmer_start+k+4)]
          if seq_kP4mer not in kmerShapeDict:
            sys.stderr.write("Error! %s not a valid %d+4-mer!\n" % (seq_kP4mer,k))
            sys.exit(1)
          newShape = kmerShapeDict[seq_kP4mer]
          nKmerShape = len(newShape)
          if seq_kmer == jKmerFwd:
            kmerShapeFeatureArray[seq_ind,j*nKmerShape:(j+1)*nKmerShape] += newShape
          elif seq_kmer == jKmerRev:
            kmerShapeFeatureArray[seq_ind,j*nKmerShape:(j+1)*nKmerShape] += newShape[::-1]

  # write dimismatchShapeDict to files
  if outputfilename is not None:
    if outputfilename[-4:] == ".npy":
      np.save(outputfilename, kmerShapeFeatureArray.toarray())
    elif outputfilename[-4:] == ".npz":
      save_sparse_csr(outputfilename, kmerShapeFeatureArray.tocsr())
    else:
      np.savetxt(outputfilename, kmerShapeFeatureArray.toarray())

  return kmerShapeFeatureArray.tocsr()

#############################################################################
# load k-mer shape kernel. rows are all training seqs,
# cols are all uniq k-mers x average shape features for all instances of that k-mer

def loadKmerShapeFeature(inputfilename):
  if inputfilename is not None:
    if inputfilename[-4:] == ".npy":
      kmerShapeFeatureArray = np.load(inputfilename)
    elif inputfilename[-4:] == ".npz":
      kmerShapeFeatureArray = load_sparse_csr(inputfilename)
    else:
      kmerShapeFeatureArray = np.loadtxt(inputfilename)

  print kmerShapeFeatureArray.shape
  return kmerShapeFeatureArray

#############################################################################
# build positional k-mer kernel.
# rows are all training seqs, cols are len(seq)*num(k-mers)

def buildPosKmerFeature(trainSeqs, k, alphabet=None,
                        kmersDict=None, kmersList=None, outputfilename=None):

  assert len(trainSeqs) > 0, "Error! Training sequence set can not be empty"
  assert k>0, "paremeter k=%d needs to be positive" % k

  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)

  nPos = len(trainSeqs[0])-k+1
  nRows = len(trainSeqs)
  nCols = len(kmersDict)*nPos

  posKmerFeatureArray = lil_matrix((nRows, nCols), dtype=np.float)

  for seq_ind in xrange(nRows):
    for seq_kmer_start in xrange(nPos):
      seq_kmer=trainSeqs[seq_ind][seq_kmer_start:(seq_kmer_start+k)]
      if seq_kmer not in kmersDict:
        sys.stderr.write("Error! %s not a valid %d-mer!\n" % (seq_kmer,k))
        sys.exit(1)
      j = len(kmersDict)*seq_kmer_start + kmersDict[seq_kmer]
      posKmerFeatureArray[seq_ind,j] = 1

  # write feature matrix to npy file
  if outputfilename is not None:
    if outputfilename[-4:] == ".npy":
      np.save(outputfilename, posKmerFeatureArray.toarray())
    elif outputfilename[-4:] == ".npz":
      save_sparse_csr(outputfilename, posKmerFeatureArray.tocsr())
    else:
      np.savetxt(outputfilename, posKmerFeatureArray.toarray())

  return posKmerFeatureArray.tocsr()

#############################################################################
# load positional k-mer kernel.
# rows are all training seqs, cols are len(seq)*num(k-mers)

def loadPosKmerFeature(inputfilename):
  if inputfilename is not None:
    if inputfilename[-4:] == ".npy":
      posKmerFeatureArray = np.load(inputfilename)
    elif inputfilename[-4:] == ".npz":
      posKmerFeatureArray = load_sparse_csr(inputfilename)
    else:
      posKmerFeatureArray = np.loadtxt(inputfilename)

  return posKmerFeatureArray


#############################################################################
# calculate di-mismatch hamming distance between all k-mers and all uniq k-mers
# rows are all k-mers and cols are all uniq k-mers
# reverse complement distance is not considered here
# dynamic programming that depends on k-1 if k>2

def buildDimismatchDist(k, alphabet=None,
                        uniqKmersDict=None, uniqKmersList=None,
                        kmersDict=None, kmersList=None,
                        pDimismatchDist=None,
                        pUniqKmersDict=None, pUniqKmersList=None,
                        pKmersDict=None, pKmersList=None,
                        outputfilename=None):

  assert k>1, "paremeter k=%d needs to be positive" % k

  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
  if uniqKmersDict is None or uniqKmersList is None:
    uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,
                                    kmersDict=kmersDict, kmersList=kmersList)

  nRows=len(kmersList)
  nCols=len(uniqKmersList)
  dimismatchDist = np.zeros(shape=(nRows,nCols))

  if k == 2:
    for i_kmer_ind in xrange(nRows):
      i_kmer = kmersList[i_kmer_ind]
      for j_uniqKmer_ind in xrange(nCols):
        j_uniqKmer = uniqKmersList[j_uniqKmer_ind]
        dimismatchDist[i_kmer_ind,j_uniqKmer_ind] = diHamming(i_kmer,j_uniqKmer)

  else: # k>2
    if pDimismatchDist is None:
      pDimismatchDist = buildDimismatchDist(k-1)

    if pKmersDict is None or pKmersList is None:
      pKmersDict,pKmersList = generateKmer(k=k-1,alphabet=alphabet)
    if pUniqKmersDict is None or pUniqKmersList is None:
      pUniqKmersDict,pUniqKmersList = generateUniqKmer(k=k-1,alphabet=alphabet,
                                        kmersDict=pKmersDict,kmersList=pKmersList)
     
    for i_kmer_ind in xrange(nRows):
      i_kmer = kmersList[i_kmer_ind]
      i_pKmer = i_kmer[:-1]
      i_pKmer_ind = pKmersDict[i_pKmer]
#      print i_kmer_ind, i_kmer, i_pKmer, i_pKmer_ind
      for j_uniqKmer_ind in xrange(nCols):
        j_uniqKmer = uniqKmersList[j_uniqKmer_ind]
        j_pUniqKmer = j_uniqKmer[:-1]
        if j_pUniqKmer in pUniqKmersDict:
          j_pUniqKmer_ind = pUniqKmersDict[j_pUniqKmer][0]
          pDist = pDimismatchDist[i_pKmer_ind,j_pUniqKmer_ind]
          if i_kmer[k-1] == j_uniqKmer[k-1] and i_kmer[k-2] == j_uniqKmer[k-2]:
            dimismatchDist[i_kmer_ind,j_uniqKmer_ind] = pDist
          else:
            dimismatchDist[i_kmer_ind,j_uniqKmer_ind] = pDist+1
#          print i_kmer_ind, i_kmer, i_pKmer, i_pKmer_ind
#          print j_uniqKmer_ind, j_uniqKmer, j_pUniqKmer, j_pUniqKmer_ind
#          print pDist, dimismatchDist[i_kmer_ind,j_uniqKmer_ind]
        else: # compare i' vs j' instead
          i_rKmer = reverseComplement(i_kmer)
          i_rKmer_ind = kmersDict[i_rKmer]
          i_prKmer = i_rKmer[1:]
          i_prKmer_ind = pKmersDict[i_prKmer]
          j_rUniqKmer = reverseComplement(j_uniqKmer)
          j_prUniqKmer = j_rUniqKmer[1:]
          if j_prUniqKmer not in pUniqKmersDict:
            sys.stderr.write("Error! %s is not a uniq k-mer.\n" % j_prUniqKmer)
            sys.exit(1)
          j_prUniqKmer_ind = pUniqKmersDict[j_prUniqKmer][0]
          pDist = pDimismatchDist[i_prKmer_ind,j_prUniqKmer_ind]
          if i_rKmer[0] == j_rUniqKmer[0] and i_rKmer[1] == j_rUniqKmer[1]:
            dimismatchDist[i_kmer_ind,j_uniqKmer_ind] = pDist
          else:
            dimismatchDist[i_kmer_ind,j_uniqKmer_ind] = pDist+1
#          print i_kmer_ind, i_kmer, j_uniqKmer_ind, j_uniqKmer
#          print i_rKmer_ind, i_rKmer, i_prKmer, i_prKmer_ind
#          print j_prUniqKmer_ind, j_rUniqKmer, j_prUniqKmer, j_prUniqKmer_ind
#          print pDist, dimismatchDist[i_kmer_ind,j_uniqKmer_ind]

  # write dimismatchDist to npy file
  if outputfilename is not None:
    if outputfilename[-4:] == ".npy":
       np.save(outputfilename, dimismatchDist)
    else:
       np.savetxt(outputfilename, dimismatchDist)

  return dimismatchDist

#############################################################################
# load di-mismatch hamming distance between all k-mers and all uniq k-mers

def loadDimismatchDist(inputfilename):
  if inputfilename is not None:
    if inputfilename[-4:] == ".npy":
      dimismatchDist = np.load(inputfilename)
    else:
      dimismatchDist = np.loadtxt(inputfilename)

  return dimismatchDist

#############################################################################
# build dimismatch dictionary. rows are all k-mers, cols are all uniq k-mers

def buildDimismatchTable(k, m, alphabet=None,
                         uniqKmersDict=None, uniqKmersList=None,
                         kmersDict=None, kmersList=None,
                         dimismatchDist=None, #dimismatchDist(k)
                         nDimismatchTable=None, #dimismatchTable(k,m+x),x>=1
                         outputfilename=None):

  assert k>0, "paremeter k=%d needs to be positive" % k
  assert m>=0, "paremeter m=%d needs to be greater or equal to 0" % m
  assert k>m, "k=%d needs to be greater than m=%d" % (k,m)

  if dimismatchDist is not None: #dimismatchDist(k)
    if kmersDict is None or kmersList is None:
      kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
    if uniqKmersDict is None or uniqKmersList is None:
      uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,
                                      kmersDict=kmersDict, kmersList=kmersList)

    nRows=len(kmersList)
    nCols=len(uniqKmersList)
    assert (nRows,nCols) == dimismatchDist.shape, "Error! Wrong dimension of dimismatchDist (%d,%d)" % dimismatchDist.shape

    dimismatchTable=lil_matrix((nRows, nCols), dtype=np.float)
    for i_uniqKmer_ind in xrange(nCols):
      i_uniqKmer = uniqKmersList[i_uniqKmer_ind]
      i_uniqKmerRevInds = uniqKmersDict[i_uniqKmer]
      i_kmerFwdInd = i_uniqKmerRevInds[1]
      i_kmerRevInd = i_uniqKmerRevInds[len(i_uniqKmerRevInds)-1]
      for j in xrange(nCols):
        dist = min(dimismatchDist[i_kmerFwdInd,j],dimismatchDist[i_kmerRevInd,j])
        if dist <= m:
          dimismatchTable[i_kmerFwdInd,j] = 1 - dist/(k-1.0)
          dimismatchTable[i_kmerRevInd,j] = 1 - dist/(k-1.0)

  elif nDimismatchTable is not None: #dimismatchTable(k,m+x),x>=1
    dimismatchTable = nDimismatchTable.copy()
    rows, cols = nDimismatchTable.nonzero()
    minDist = 1 - m/(k-1.0)
    for i, j in zip(rows, cols):
      value = nDimismatchTable[i,j]
      if value < minDist:
        dimismatchTable[i,j] = 0
#        print i,j,value
    dimismatchTable = dimismatchTable.tocsr().eliminate_zeros()

  else:
    if kmersDict is None or kmersList is None:
      kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
    if uniqKmersDict is None or uniqKmersList is None:
      uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,kmersDict=kmersDict, kmersList=kmersList)

    nRows=len(kmersList)
    nCols=len(uniqKmersList)

    dimismatchTable=lil_matrix((nRows, nCols), dtype=np.float)

    for kmer_ind in xrange(nRows):
      kmer = kmersList[kmer_ind]
      for uniqKmer_ind in xrange(nCols):
        uniqKmer = uniqKmersList[uniqKmer_ind]
        distFwd = diHamming(kmer, uniqKmer)
        uniqKmerRevInds = uniqKmersDict[uniqKmer]
        if len(uniqKmerRevInds) == 2:
  #        sys.stderr.write("%s is self reverse complimented.\n" % uniqKmer)
          distRev = distFwd
        else:
          uniqKmerRevInd = uniqKmerRevInds[2]
          uniqKmerRev = kmersList[uniqKmerRevInd]
          distRev = diHamming(kmer, uniqKmerRev)
        dist = min(distFwd, distRev)
  #      sys.stderr.write("dist(%s,%s)=%d\n" % (kmer,uniqKmer,dist))
        if dist <= m:
          dimismatchTable[kmer_ind,uniqKmer_ind] = 1 - dist/(k-1.0)

  # write dismismatchTable to npy file
  if outputfilename is not None:
    if outputfilename[-4:] == ".npy":
      np.save(outputfilename, dimismatchTable.toarray())
    elif outputfilename[-4:] == ".npz":
      save_sparse_csr(outputfilename, dimismatchTable.tocsr())
    else:
      np.savetxt(outputfilename, dimismatchTable.toarray())

  return dimismatchTable.tocsr()

#############################################################################
# load dimismatch dictionary. rows are all k-mers, cols are all uniq k-mers

def loadDimismatchTable(inputfilename):
  if inputfilename is not None:
    if inputfilename[-4:] == ".npy":
      dimismatchTable = np.load(inputfilename)
    elif inputfilename[-4:] == ".npz":
      dimismatchTable = load_sparse_csr(inputfilename)
    else:
      dimismatchTable = np.loadtxt(inputfilename)

  return dimismatchTable

#############################################################################
# build kmer shape dictionary, key is shapeType, values are kmer shape matrix
# shape matrix rows are all k-mers, cols are all uniq k-mers

def buildDimismatchDNAShapeTable(k, m, alphabet=None,
                                 dimismatchTable=None, kmerShapeDict=None,
                                 uniqKmersDict=None, uniqKmersList=None,
                                 kmersDict=None, kmersList=None,
                                 outputfilename=None):

  assert k>0, "paremeter k=%d needs to be positive" % k
  assert m>=0, "paremeter m=%d needs to be greater or equal to 0" % m
  assert k>m, "k=%d needs to be greater than m=%d" % (k,m)

  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
  if uniqKmersDict is None or uniqKmersList is None:
    uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,kmersDict=kmersDict, kmersList=kmersList)

  nRows=len(kmersList)
  nCols=len(uniqKmersList)
  if kmerShapeDict is None:
#    kmerShapeDict=calculateKmerDNAShape(k=k, alphabet=alphabet, kmersDict=kmersDict, kmersList=kmersList)
    kmerShapeDict=calculateKmerDNAShape(k=k+4, alphabet=alphabet, flanking=True)
#  for key in kmerShapeDict:
#    print key, kmerShapeDict[key], len(kmerShapeDict[key])

  if dimismatchTable is None:
    dimismatchTable=buildDimismatchTable(k=k, m=m, alphabet=alphabet,
                    uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                    kmersDict=kmersDict, kmersList=kmersList)

  dimismatchShapeDict={}
  for shapeType in shapeTypes:
    dimismatchShapeDict[shapeType]=lil_matrix((nRows, nCols), dtype=np.float)

#  for i in xrange(nRows):
#    for j in xrange(nCols):
#      if dimismatchTable[i,j] > 0:
  rows,cols = dimismatchTable.nonzero()
  for i, j in zip(rows,cols):
    # find out kmers at i and j and calculate their inner dot product of shape vectors
    iKmer=kmersList[i]
    newiKmer="NN"+iKmer+"NN"
#    sys.stderr.write("i=%d,iKmer=%s,newiKmer=%s\n" % (i,iKmer,newiKmer))
    jKmerFwd=uniqKmersList[j]
    newjKmerFwd="NN"+jKmerFwd+"NN"
#    sys.stderr.write("j=%d,jKmerFwd=%s,newjKmerFwd=%s\n" % (j,jKmerFwd,newjKmerFwd))
    jKmerInds=uniqKmersDict[jKmerFwd]
    jKmerFwdInd=jKmerInds[1]
    if len(jKmerInds) == 2:
      jKmerRevInd = jKmerFwdInd
    else:
      jKmerRevInd=jKmerInds[2]
    jKmerRev=kmersList[jKmerRevInd]
    newjKmerRev="NN"+jKmerRev+"NN"
#    sys.stderr.write("j=%d,jKmerFwdInd=%d,jKmerRevInd=%d,jKmerRev=%s,newiKmerRev=%s\n" % (j,jKmerFwdInd,jKmerRevInd,jKmerRev,newjKmerRev))
#     sys.stderr.write("i=%d,j=%d,jKmerFwdInd=%d,jKmerRevInd=%d\n" % (i,j,jKmerFwdInd,jKmerRevInd))
    
    shapeColInd=0
    for shapeType in shapeTypes:
      if shapeType == "MGW" or shapeType == "ProT":
        nCols=k-4+4
      else:
        nCols=k-3+4
      
#      iKmerShape=kmerShapeDict[shapeType][i]
      iKmerShape=kmerShapeDict[newiKmer][shapeColInd:(shapeColInd+nCols)]
      iKmerShapeNorm=np.linalg.norm(iKmerShape)
#      jKmerFwdShape=kmerShapeDict[shapeType][jKmerFwdInd]
      jKmerFwdShape=kmerShapeDict[newjKmerFwd][shapeColInd:(shapeColInd+nCols)]
      jKmerFwdShapeNorm=np.linalg.norm(jKmerFwdShape)
#      jKmerRevShape=kmerShapeDict[shapeType][jKmerRevInd][::-1]
      jKmerRevShape=kmerShapeDict[newjKmerRev][shapeColInd:(shapeColInd+nCols)]
      jKmerRevShapeNorm=np.linalg.norm(jKmerRevShape)
      scalerFwd=np.dot(iKmerShape,jKmerFwdShape)/iKmerShapeNorm/jKmerFwdShapeNorm
      scalerRev=np.dot(iKmerShape,jKmerRevShape)/iKmerShapeNorm/jKmerRevShapeNorm
      dimismatchShapeDict[shapeType][i,j]=max(scalerFwd,scalerRev)
#      print shapeType, shapeColInd, scalerFwd,scalerRev

      shapeColInd+=nCols

  for shapeType in shapeTypes:
    dimismatchShapeDict[shapeType] = dimismatchShapeDict[shapeType].tocsr()

  # write dimismatchShapeDict to files
  if outputfilename is not None:
    for shapeType in shapeTypes:
      outputfile=outputfilename+"."+shapeType+".npz"
      save_sparse_csr(outputfile, dimismatchShapeDict[shapeType].tocsr())

  return dimismatchShapeDict

#############################################################################
# load kmer shape dictionary, key is shapeType, values are kmer shape matrix
# shape matrix rows are all k-mers, cols are all uniq k-mers

def loadDimismatchDNAShapeTable(inputfilename):
  dimismatchShapeDict = {}
  for shapeType in shapeTypes:
    inputfile=inputfilename+"."+shapeType+".npz"
    if not os.path.isfile(inputfile):
      sys.stderr.write("Error! file %s not exist!\n" % inputfile)
      sys.exit(1)
    dimismatchShapeDict[shapeType] = load_sparse_csr(inputfile)

  return dimismatchShapeDict

#############################################################################
# build dimismatch kernel. rows are all training seqs, cols are all uniq k-mers

def buildDimismatchFeature(trainSeqs, k, m, alphabet=None,
                          uniqKmersDict=None, uniqKmersList=None,
                          kmersDict=None, kmersList=None,
                          dimismatchTable=None,outputfilename=None):

  assert len(trainSeqs) > 0, "Error! Training sequence set can not be empty"
  if dimismatchTable is None:
    dimismatchTable = buildDimismatchTable(k=k, m=m, alphabet=alphabet,
                                           uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                           kmersDict=kmersDict, kmersList=kmersList)
  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
  if uniqKmersDict is None or uniqKmersList is None:
    uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,
                                    kmersDict=kmersDict, kmersList=kmersList)

  nRows = len(trainSeqs)
  nCols = len(uniqKmersDict)

  dimismatchFeatureArray = lil_matrix((nRows, nCols), dtype=np.float)

  for seq_ind in xrange(nRows):
    for seq_kmer_start in xrange(len(trainSeqs[0])-k+1):
      seq_kmer=trainSeqs[seq_ind][seq_kmer_start:(seq_kmer_start+k)]
      if seq_kmer not in kmersDict:
        sys.stderr.write("Error! %s not a valid %d-mer!\n" % (seq_kmer,k))
        sys.exit(1)
      seq_kmer_ind = kmersDict[seq_kmer]
#      sys.stderr.write("Look up for %s => %d\n" % (seq_kmer, seq_kmer_ind))
      dimismatchFeatureArray[seq_ind,:] += dimismatchTable[seq_kmer_ind,:]

  # write feature matrix to npy file
  if outputfilename is not None:
    if outputfilename[-4:] == ".npy":
      np.save(outputfilename, dimismatchFeatureArray.toarray())
    elif outputfilename[-4:] == ".npz":
      save_sparse_csr(outputfilename, dimismatchFeatureArray.tocsr())
    else:
      np.savetxt(outputfilename, dimismatchFeatureArray.toarray())

  return dimismatchFeatureArray.tocsr()

#############################################################################
# load dimismatch kernel. rows are all training seqs, cols are all uniq k-mers

def loadDimismatchFeature(inputfilename):
  if inputfilename is not None:
    if inputfilename[-4:] == ".npy":
      dimismatchFeatureArray = np.load(inputfilename)
    elif inputfilename[-4:] == ".npz":
      dimismatchFeatureArray = load_sparse_csr(inputfilename)
    else:
      dimismatchFeatureArray = np.loadtxt(inputfilename)

  return dimismatchFeatureArray

#############################################################################
# build dimismatch shape kernel. rows are all training seqs, cols are all uniq k-mers

def buildDimismatchShapeFeature(trainSeqs, k, m, alphabet=None,
                               uniqKmersDict=None, uniqKmersList=None,
                               kmersDict=None, kmersList=None,
                               dimismatchTable=None, dimismatchShapeDict=None,
                               outputfilename=None):

  assert len(trainSeqs) > 0, "Error! Training sequence set can not be empty"
  if dimismatchTable is None:
    dimismatchTable = buildDimismatchTable(k=k, m=m, alphabet=alphabet,
                                           uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                           kmersDict=kmersDict, kmersList=kmersList)
  if dimismatchShapeDict is None:
    dimismatchShapeDict = buildDimismatchDNAShapeTable(k, m,
                            dimismatchTable=dimismatchTable)

  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k=k,alphabet=alphabet)
  if uniqKmersDict is None or uniqKmersList is None:
    uniqKmersDict,uniqKmersList = generateUniqKmer(k=k,alphabet=alphabet,
                                    kmersDict=kmersDict, kmersList=kmersList)

  nRows = len(trainSeqs)
  nCols = len(uniqKmersDict)

  dimismatchShapeFeatureArray = lil_matrix((nRows, nCols*(1+nShape)), dtype=np.float)

  for seq_ind in xrange(nRows):
    for seq_kmer_start in xrange(len(trainSeqs[0])-k+1):
      seq_kmer=trainSeqs[seq_ind][seq_kmer_start:(seq_kmer_start+k)]
      if seq_kmer not in kmersDict:
        sys.stderr.write("Error! %s not a valid %d-mer!\n" % (seq_kmer,k))
        sys.exit(1)
      seq_kmer_ind = kmersDict[seq_kmer]
#      sys.stderr.write("Look up for %s => %d\n" % (seq_kmer, seq_kmer_ind))
      dimismatchShapeFeatureArray[seq_ind,0:nCols] += dimismatchTable[seq_kmer_ind,:]
      for shapeInd in xrange(nShape):
        shapeType = shapeTypes[shapeInd]
        dimismatchShapeFeatureArray[seq_ind,nCols*(shapeInd+1):nCols*(shapeInd+2)] += dimismatchShapeDict[shapeType][seq_kmer_ind,:]

  # write feature matrix to npy file
  if outputfilename is not None:
    if outputfilename[-4:] == ".npy":
      np.save(outputfilename, dimismatchShapeFeatureArray.toarray())
    elif outputfilename[-4:] == ".npz":
      save_sparse_csr(outputfilename, dimismatchShapeFeatureArray.tocsr())
    else:
      np.savetxt(outputfilename, dimismatchShapeFeatureArray.toarray())

  return dimismatchShapeFeatureArray.tocsr()

#############################################################################
# load dimismatch shape kernel. rows are all training seqs, cols are all uniq k-mers

def loadDimismatchShapeFeature(inputfilename):
  if inputfilename is not None:
    if inputfilename[-4:] == ".npy":
      dimismatchShapeFeatureArray = np.load(inputfilename)
    elif inputfilename[-4:] == ".npz":
      dimismatchShapeFeatureArray = load_sparse_csr(inputfilename)
    else:
      dimismatchShapeFeatureArray = np.loadtxt(inputfilename)

  return dimismatchShapeFeatureArray

#############################################################################
# call dnashape binary to get shape information of input seq and store in dict
# the sequences can not contain N

def callDNAShapePrediction(seqfilename, seqs=None, k=None, seqNum=None):
  seqfilename_copy = seqfilename
  if seqfilename is None:
    assert seqs is not None
    seqfilename = "seq.temp"
    seqfile = open(seqfilename,"r")
    seqs=[]
    for line in seqfile:
      seqs.append(line.rstrip())
    sys.stderr.write("load %d sequences from %s\n" % (len(seqs), seqfilename))
    seqfile.close()

  if seqfilename[-3:] == ".gz":
    if not os.path.isfile(seqfilename[:-3]):
      commandLine="gunzip -c %s > %s" % (seqfilename, seqfilename[:-3])
      utils.runCommand(commandLine)
    seqfilename = seqfilename[:-3]

  if not os.path.isfile(seqfilename+".MGW"):
    commandLine="../bin/v2.5_noSim/prediction -i %s -width 100" % seqfilename
    utils.runCommand(commandLine)

  if seqfilename == "seq.temp":
    commandLine="rm %s" % seqfilename
    utils.runCommand(commandLine)

  fp = fileinput.input(seqfilename)
  for line in fp:
    if fp.isfirstline():
      if k is not None:
        assert k==len(line.rstrip()), "k=%d doesn't match input sequence length %d" % (k,len(line.rstrip()))
      k=len(line.rstrip())
      sys.stderr.write("sequence length=%d\t" % k)
  if seqNum is not None:
    assert seqNum==fp.filelineno(), "seqNum=%d doesn't match input sequence number %d" % (seqNum, fp.filelineno())
  seqNum = fp.filelineno()
  sys.stderr.write("line number=%d\n" % seqNum)

  shapeDict={}
  nRows=seqNum
  for shapeType in shapeTypes:
    if shapeType == "MGW" or shapeType == "ProT":
      nCols=k-4
    else:
      nCols=k-3
    shapeDict[shapeType]=np.zeros(shape=(nRows,nCols),dtype=np.float)
    shapefilename=seqfilename+"."+shapeType
    shapefile = open(shapefilename, "r")
    row_ind = 0
    for line in shapefile:
      if line[0] == ">":
        continue
      words=line.rstrip().split(",")
      col_ind = 0
      for word in words:
        if word != "NA":
          shapeDict[shapeType][row_ind,col_ind] = float(word)
          col_ind += 1
      row_ind += 1

  # clean up files
#  commandLine="rm %s*" % seqfilename
#  utils.runCommand(commandLine)

  return shapeDict

#############################################################################
# load 5-mer DNA shape look up table
# key is the 5mer, info is the shape vector
# FWD 5mer and REV 5mer are saved as separate keys
# Roll and HelT have 2 shape values
# if flanking = True, appending up to 2N flanking sequences in the table

def loadShapeTable(flanking=False):

  shapeFilename = "../data/lookup-table/shape.5mer.txt"
#  k = 5
#  kmersDict,kmersList = generateKmer(k, alphabet=None, outputfilename=None)
#  uniqKmersDict,uniqKmersList = generateUniqKmer(k=k, alphabet=None, kmersDict=kmersDict, kmersList=kmersList)
  shapeDict = {}
  shapeFile = open(shapeFilename)
  for line in shapeFile:
    words = line.rstrip().split("\t")
    assert len(words) == 8, "Wrong lookup table format. %s" % shapeFilename
    fwdKmer = words[0]
    if fwdKmer == "#FWD": # first header line
      continue
    revKmer = words[1]
    MGW = float(words[2])
    ProT = float(words[3])
    Roll1 = float(words[4])
    Roll2 = float(words[5])
    HelT1 = float(words[6])
    HelT2 = float(words[7])
#    shapeDict[fwdKmer] = np.array(float(words[2]),float(words[3]),float(words[4]),float(words[5]))
    shapeDict[fwdKmer] = (MGW, ProT, Roll1, Roll2, HelT1, HelT2)
    shapeDict[revKmer] = (MGW, ProT, Roll2, Roll1, HelT2, HelT1)
  shapeFile.close()
  sys.stderr.write("Load shape look up table for all 5mers.\n")

  if flanking is True:

    t4mersDict,t4mersList = generateKmer(4, alphabet=None, outputfilename=None)
    for t4mer in t4mersList:
      # N+(k-1)mer
      newKmer = "N"+t4mer
      kmerShape = np.zeros(6)
      for char in "ACGT":
        kmer = char+t4mer
        kmerShape += shapeDict[kmer]
      kmerShape /= 4.0
      shapeDict[newKmer] = kmerShape
      # (k-1)mer+N
      newKmerRevComp=reverseComplement(newKmer)
      shapeDict[newKmerRevComp] = kmerShape
    nFlankingSeqs = 2*len(t4mersList)
    sys.stderr.write("Added %d N-flanking sequences into the DNA shape look up tabe.\n" % nFlankingSeqs)

    t3mersDict,t3mersList = generateKmer(3, alphabet=None, outputfilename=None)
    for t3mer in t3mersList:
      # NN+(k-2)mer
      newKmer = "NN"+t3mer
      kmerShape = np.zeros(6)
      for char1 in "ACGT":
        for char2 in "ACGT":
          kmer = char1+char2+t3mer
          kmerShape += shapeDict[kmer]
      kmerShape /= 16.0
      shapeDict[newKmer] = kmerShape
      # (k-2)mer+NN
      newKmerRevComp=reverseComplement(newKmer)
      shapeDict[newKmerRevComp] = kmerShape
      # N+(k-2)mer+N
      newKmer = "N"+t3mer+"N"
      kmerShape = np.zeros(6)
      for char1 in "ACGT":
        for char2 in "ACGT":
          kmer = char1+t3mer+char2
          kmerShape += shapeDict[kmer]
      kmerShape /= 16.0
      shapeDict[newKmer] = kmerShape
    nFlankingSeqs = 3*len(t3mersList)
    sys.stderr.write("Added %d NN-flanking sequences into the DNA shape look up tabe.\n" % nFlankingSeqs)

    t2mersDict,t2mersList = generateKmer(2, alphabet=None, outputfilename=None)
    for t2mer in t2mersList:
      # NN+(k-3)+N mer
      newKmer = "NN"+t2mer+"N"
      kmerShape = np.zeros(6)
      for char1 in "ACGT":
        for char2 in "ACGT":
            for char3 in "ACGT":
              kmer = char1+char2+t2mer+char3
              kmerShape += shapeDict[kmer]
      kmerShape /= 64.0
      shapeDict[newKmer] = kmerShape
      # N+(k-2)mer+NN
      newKmerRevComp=reverseComplement(newKmer)
      shapeDict[newKmerRevComp] = kmerShape
    nFlankingSeqs = 2*len(t2mersList)
    sys.stderr.write("Added %d NN+N-flanking sequences into the DNA shape look up tabe.\n" % nFlankingSeqs)

    t1mersDict,t1mersList = generateKmer(1, alphabet=None, outputfilename=None)
    for t1mer in t1mersList:
      # NN+(k-1)mer+NN
      newKmer = "NN"+t1mer+"NN"
      kmerShape = np.zeros(6)
      for char1 in "ACGT":
        for char2 in "ACGT":
          for char3 in "ACGT":
            for char4 in "ACGT":
              kmer = char1+char2+t1mer+char3+char4
              kmerShape += shapeDict[kmer]
      kmerShape /= 256.0
      shapeDict[newKmer] = kmerShape
    nFlankingSeqs = 1*len(t1mersList)
    sys.stderr.write("Added %d NN+NN-flanking sequences into the DNA shape look up tabe.\n" % nFlankingSeqs)

  return shapeDict

#############################################################################
# calculate dnashape values of given sequences using dnashape look up table
# if flanking=True, the sequences can contain up to 2 Ns in the flanking sequences 

def calculateDNAShape(seqfilename=None, seqs=None, k=None, seqNum=None, shapeDict=None, flanking=False):
  if seqs is None:
    assert seqfilename is not None or os.path.isfile(seqfilename), "seqfilename=%s does not exist." % seqfilename 
    seqfile = open(seqfilename,"r")
    seqs=[]
    for line in seqfile:
      seqs.append(line.rstrip())
    sys.stderr.write("load %d sequences from %s\n" % (len(seqs), seqfilename))
    seqfile.close()
    return calculateDNAShape(seqs=seqs, k=k, seqNum=seqNum, shapeDict=shapeDict, flanking=flanking)

  if shapeDict is None:
    shapeDict = loadShapeTable(flanking=flanking)

  if seqNum is not None:
    assert seqNum == len(seqs), "seqNum=%d doesn't match input sequence number %d" % (seqNum, len(seqs))
  else:
    seqNum = len(seqs)
  assert seqNum >0 , "seqNum=%d needs to be positive" % seqNum

  if k is not None:
    assert k == len(seqs[0]), "k=%d doesn't match input sequence length %d" % (k, len(seqs[0]))
  else:
    k = len(seqs[0])

  seqShapeDict={}
  nRows=seqNum
  for shapeType in shapeTypes:
    if shapeType == "MGW" or shapeType == "ProT":
      nCols=k-4
    else:
      nCols=k-3
    seqShapeDict[shapeType]=np.zeros(shape=(nRows,nCols),dtype=np.float)

  for index in xrange(seqNum):
    seq= seqs[index]
    RollP = 0.0
    HelTP = 0.0
    for seq_index in xrange(k-4):
      seq5mer = seq[seq_index:seq_index+5]
      MGW, ProT, Roll1, Roll2, HelT1, HelT2 = shapeDict[seq5mer]
      seqShapeDict["MGW"][index,seq_index] = MGW
      seqShapeDict["ProT"][index,seq_index] = ProT
      if seq_index == 0:
        seqShapeDict["Roll"][index,seq_index] = Roll1
        seqShapeDict["HelT"][index,seq_index] = HelT1
      else:
        seqShapeDict["Roll"][index,seq_index] = (Roll1+RollP)/2.0
        seqShapeDict["HelT"][index,seq_index] = (HelT1+HelTP)/2.0
      RollP = Roll2
      HelTP = HelT2
    seqShapeDict["Roll"][index,k-4] = RollP
    seqShapeDict["HelT"][index,k-4] = HelTP

#  print len(seqShapeDict)
  return seqShapeDict

#############################################################################
# call dnashape binary to get shape information of all k-mers and store in dict

def callKmerDNAShapePrediction(k, alphabet=None, kmersDict=None, kmersList=None, kmerfilename=None):
  if kmersDict is None or kmersList is None:
    if not os.path.isfile(kmerfilename):
      kmersDict,kmersList = generateKmer(k, alphabet=alphabet, outputfilename=kmerfilename)
    else:
      kmersDict,kmersList = loadKmer(kmerfilename, k=k)

  return callDNAShapePrediction(seqfilename=kmerfilename, seqs=kmersList, seqNum=len(kmersList), k=k)

#############################################################################
# calcualte shape information of all k-mers and store in dict
# if flanking=True, up to 2 flanking char can be replaced by N for all k-mers 

def calculateKmerDNAShape(k, alphabet=None, kmersDict=None, kmersList=None, flanking=False):
  if kmersDict is None or kmersList is None:
    kmersDict,kmersList = generateKmer(k, alphabet=alphabet)
  
  if flanking is True:
    assert k>=3, "k needs to be greater than or equal to 3"
    kM1mersDict,kM1mersList = generateKmer(k=k-1, alphabet=alphabet)
    for kM1mer in kM1mersList:
      kmersList.append("N"+kM1mer)
      kmersList.append(kM1mer+"N")
    kM2mersDict,kM2mersList = generateKmer(k=k-2, alphabet=alphabet)
    for kM2mer in kM2mersList:
      kmersList.append("NN"+kM2mer)
      kmersList.append(kM2mer+"NN")
      kmersList.append("N"+kM2mer+"N")
    if k>3:
      kM3mersDict,kM3mersList = generateKmer(k=k-3, alphabet=alphabet)
      for kM3mer in kM3mersList:
  #      kmersList.append("NNN"+kM3mer)
  #      kmersList.append(kM3mer+"NNN")
        kmersList.append("NN"+kM3mer+"N")
        kmersList.append("N"+kM3mer+"NN")
      if k>4:
        kM4mersDict,kM4mersList = generateKmer(k=k-4, alphabet=alphabet)
        for kM4mer in kM4mersList:
    #      kmersList.append("NNNN"+kM4mer)
    #      kmersList.append(kM4mer+"NNNN")
    #      kmersList.append("NNN"+kM4mer+"N")
    #      kmersList.append("N"+kM4mer+"NNN")
          kmersList.append("NN"+kM4mer+"NN")
  shapeDict = calculateDNAShape(seqs=kmersList, seqNum=len(kmersList), k=k, flanking=flanking)

  kmerShapeDict={}
  for seq_index in xrange(len(kmersList)):
    seq = kmersList[seq_index]
    kmerShapeDict[seq] = np.hstack(shapeDict[shapeType][seq_index,:] for shapeType in shapeTypes)
#    print len(kmerShapeDict[seq])

  return kmerShapeDict
