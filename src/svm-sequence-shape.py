#!/usr/bin/env python
# AUTHOR: Wenxiu Ma
# MODIFIED DATA: 18 May 2015

from dnashape import *
from svm_utils import *
from io_utils import *

import sys
import optparse 


#############################################################################
# MAIN
#############################################################################

# Parse the command line.
### parse the command line arguments
USAGE = "usage: %prog [options]"
parser = optparse.OptionParser(usage=USAGE)
parser.add_option("-x", "--seqfile", dest="seqsFilename",
                  help="input DNA sequence file, one sequence per line")
parser.add_option("-y", "--labelfile", dest="labelFilename",
                  help="[SVM] input label file, one label per line; "
                  "or [SVR] input affinity file, one affinity value per line.")
parser.add_option("-o", "--outfile", dest="outputFilename",
                   help="output filename")
parser.add_option("-r", "--svr", dest="bSVR", default=False, action="store_true",
                  help="OPTIONAL: use SVR regression instead of SVM classification")
parser.add_option("-p", "--nt", dest="p", type="int", default=None,
                  help="OPTIONAL: use positional k-mer")
parser.add_option("-k", "--kmer", dest="k", type="int", default=None,
                  help="OPTIONAL: use compositional k-mer")
parser.add_option("-m", "--mismatch", dest="m", type="int", default=None,
                  help="OPTIONAL: maximum number of mismatches allowed, for di-mismatch or di-mismatch+shape kernel")
parser.add_option("-s", "--shape", dest="shape", default=None,
                  help="OPTIONAL: type of DNA shape parameters to use: MGW, Roll, ProT, HelT, shape")
parser.add_option("-f", "--feature_selection", dest="maxFeatureNum", type="int", default=None,
                  help="OPTIONAL: maximum number of features to be selected")
parser.add_option("-c", "--cross_validation_fold", dest="cvFold", type="int", default=3,
                  help="OPTIONAL: c-fold nested cross validation; DEFAULT: %default")
parser.add_option("-t", "--cross_validation_fold_ind", dest="cvFoldInd", type="int", default=None,
                  help="OPTIONAL: index of desired outer cross validation")
parser.add_option("-d", "--data-dir", dest="dataDir", default=None,
                  help="OPTIONAL: input database directory for precomputed k-mer data")
parser.add_option("-q", "--output-dir", dest="outputDir", default=None,
                  help="OPTIONAL: output results directory")

(options, args) = parser.parse_args()
if len(args) != 0:
  parser.error("incorrect number of arguments")
if not options.seqsFilename:
  parser.error('input sequence file -x not given') 
if not options.labelFilename:
  parser.error('input label/affinity file -y not given') 
if not options.outputFilename:
  parser.error('output filename -o not given') 

printTime("Program started")

# load input sequences
seqFile = open(options.seqsFilename, "r")
seqs = [line.rstrip() for line in seqFile]
seqFile.close()

# load input labels
labels = np.loadtxt(options.labelFilename)

assert len(seqs) == len(labels), "Error! x and y are not of same length!"
assert len(seqs)>0, "Error! Empty input sequences!"
seqLen = len(seqs[0])

bSVR = options.bSVR
p = options.p
k = options.k
m = options.m
shape = options.shape
maxFeatureNum = options.maxFeatureNum
assert shape is None or shape in shapeTypes \
       or shape == "shape", "Invalid input shape parameter: %s!" % shape

outputFile = open(options.outputFilename, "w")

if p is not None:
  assert p>0, "parameter p=%d needs to be positive" % p

  if shape is not None:
    # pos-kmer + shape kernel
    sys.stderr.write("pos-kmer + shape kernel: p=%d shape=%s\n" % (p,shape))
    kmersDict,kmersList = getKmer(p,dir=options.dataDir)
    posKmerFeatureArray = getPosKmerFeature(options.seqsFilename, p, seqs=seqs, 
                                      kmersDict=kmersDict, kmersList=kmersList, 
                                      dir=options.outputDir)
    shapeFeatureArray = getShapeFeature(options.seqsFilename, shape=shape, seqs=seqs)
    print shapeFeatureArray.shape

    if sparse.issparse(posKmerFeatureArray):
      featureArray = sparse.hstack([posKmerFeatureArray,shapeFeatureArray]).tocsr()
    else:
      featureArray = np.concatenate([posKmerFeatureArray,shapeFeatureArray],axis=1)

    expName = "%dnt+%s" % (p,shape)
    featureNames = []
    for pos in xrange(seqLen-p+1):
      featureNames.extend(["%d#%s" % (pos, item) for item in kmersList])
    if shape == "shape":
      for shapeType in shapeTypes:
        if shapeType == "MGW" or shapeType == "ProT":
          nCols=seqLen-4
        else:
          nCols=seqLen-3
        featureNames.extend(["%s#%d" % (shapeType, index) for index in xrange(nCols)])
    else:
      featureNames.extend(["%s#%d" % (shape, index) for index in xrange(shapeFeatureArray.shape[1])])
#    print featureNames

  else:
    # pos-kmer kernel
    sys.stderr.write("pos-kmer kernel: p=%d\n" % (p))
    kmersDict,kmersList = getKmer(p,dir=options.dataDir)
    posKmerFeatureArray = getPosKmerFeature(options.seqsFilename, p, seqs=seqs, 
                                      kmersDict=kmersDict, kmersList=kmersList, 
                                      dir=options.outputDir)
    featureArray = posKmerFeatureArray
    expName = "%dnt" % p
    featureNames = []
    for pos in xrange(seqLen-p+1):
      featureNames.extend(["%d#%s" % (pos, item) for item in kmersList])
#    print featureNames

elif k is not None:
  assert k>0, "parameter k=%d needs to be positive" % k

  if m is not None:
    assert m>0, "parameter m=%d needs to be positive" % m
    assert k>=m, "parameters k=%d needs to be greater or equal to m=%d" % (k,m)
    assert k>=3, "parameter k=%d needs to be greater than or equal to 3 for dimismatch kernel" % k

    if shape is not None:
      # dimismatch + shape kernel
      sys.stderr.write("dimismatch + shape kernel: k=%d m=%d shape=%s\n" % (k,m,shape))
      kmersDict,kmersList = getKmer(k,dir=options.dataDir)
      uniqKmersDict,uniqKmersList = getUniqKmer(k, kmersDict=kmersDict, kmersList=kmersList, dir=options.dataDir)
      dimismatchFeatureArray = getDimismatchFeature(options.seqsFilename, seqs, k, m,
                                 kmersDict=kmersDict, kmersList=kmersList, uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                 indir=options.dataDir, outdir=options.outputDir)
      dimismatchShapeFeatureArray = getDimismatchShapeFeature(options.seqsFilename, seqs, k, m, shape,
                                      kmersDict=kmersDict, kmersList=kmersList, uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                      indir=options.dataDir, outdir=options.outputDir)
      if sparse.issparse(dimismatchShapeFeatureArray):
        featureArray = sparse.hstack([dimismatchFeatureArray,dimismatchShapeFeatureArray]).tocsr()
      else:
        featureArray = np.concatenate([dimismatchFeatureArray,dimismatchShapeFeatureArray],axis=1)

      expName = "%dmer.%dm.%s" % (k,m,shape)
      featureNames = []
      featureNames.extend(uniqKmersList)
#      print len(featureNames)
#      print shapeTypes
      if shape == "shape":
        for shapeType in shapeTypes:
          featureNames.extend(["%s.%s" % (item, shapeType) for item in uniqKmersList])
#          print shapeType
#          print len(featureNames)
      else:
        featureNames.extend(["%s.%s" % (item, shape) for item in uniqKmersList])
#      print len(featureNames)
#      print featureNames

    else:
      # dimismatch kernel
      sys.stderr.write("dimismatch kernel: k=%d m=%d\n" % (k,m))
      kmersDict,kmersList = getKmer(k,dir=options.dataDir)
      uniqKmersDict,uniqKmersList = getUniqKmer(k, kmersDict=kmersDict, kmersList=kmersList, dir=options.dataDir)
      dimismatchFeatureArray = getDimismatchFeature(options.seqsFilename, seqs, k, m,
                                 kmersDict=kmersDict, kmersList=kmersList, uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                 indir=options.dataDir, outdir=options.outputDir)
      featureArray = dimismatchFeatureArray
      expName = "%dmer.%dm" % (k,m)
      featureNames = uniqKmersList
#      print featureNames

  elif shape is not None:
    # k-mer + shape kernel - 2015/12 updated
    sys.stderr.write("kmer + shape kernel: k=%d shape=%s\n" % (k,shape))
    kmersDict,kmersList = getKmer(k,dir=options.dataDir)
    uniqKmersDict,uniqKmersList = getUniqKmer(k, kmersDict=kmersDict, kmersList=kmersList, dir=options.dataDir)
    kmerFeatureArray = getKmerFeature(options.seqsFilename, k, seqs=seqs, 
                                      kmersDict=kmersDict, kmersList=kmersList, uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                      dir=options.outputDir)
#    shapeFeatureArray = getShapeFeature(options.seqsFilename, shape=shape, seqs=seqs)
    shapeFeatureArray = getKmerShapeFeature(options.seqsFilename, k, shape=shape, seqs=seqs,
                          kmersDict=kmersDict, kmersList=kmersList, uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                          dir=options.outputDir)

    if sparse.issparse(kmerFeatureArray):
      featureArray = sparse.hstack([kmerFeatureArray,shapeFeatureArray]).tocsr()
    else:
      featureArray = np.concatenate([kmerFeatureArray,shapeFeatureArray],axis=1)
    
    expName = "%dmer+%s" % (k,shape)
    featureNames = []
    featureNames.extend(uniqKmersList) # should not set featureNames = uniqKmersList since that's ref by name
    sys.stdout.flush()
    nCol, shapeIndexDict = calculateShapeIndex(k)
    print nCol, shapeIndexDict
    if shape == "shape":
      for uniqKmer in uniqKmersList:
        for shapeType in shapeTypes:
#        if shapeType == "MGW" or shapeType == "ProT":
#          nCols=seqLen-4
#        else:
#          nCols=seqLen-3
#        featureNames.extend(["%s#%d" % (shapeType, index) for index in xrange(nCols)])
          shapeStart,shapeLen = shapeIndexDict[shapeType]
          featureNames.extend(["%s.%s#%d" % (uniqKmer, shapeType, pos) for pos in xrange(shapeLen)])
    else:
#      featureNames.extend(["%s#%d" % (shape, index) for index in xrange(shapeFeatureArray.shape[1])])
      shapeStart,shapeLen = shapeIndexDict[shape]
      for uniqKmer in uniqKmersList:
        featureNames.extend(["%s.%s#%d" % (uniqKmer, shape, pos) for pos in xrange(shapeLen)])
#    print featureNames
    sys.stdout.flush()

  else:
    # k-mer kernel
    sys.stderr.write("kmer kernel: k=%d\n" % (k))
    kmersDict,kmersList = getKmer(k,dir=options.dataDir)
    uniqKmersDict,uniqKmersList = getUniqKmer(k, kmersDict=kmersDict, kmersList=kmersList, dir=options.dataDir)
    kmerFeatureArray = getKmerFeature(options.seqsFilename, k, seqs=seqs, 
                                      kmersDict=kmersDict, kmersList=kmersList, uniqKmersDict=uniqKmersDict, uniqKmersList=uniqKmersList,
                                      dir=options.outputDir)
    featureArray = kmerFeatureArray
    expName = "%dmer" % k
    featureNames = uniqKmersList
#    print featureNames

elif shape is not None:
  # shape kernel
  sys.stderr.write("shape kernel: shape=%s\n" % (shape))
  shapeFeatureArray = getShapeFeature(options.seqsFilename, shape=shape, seqs=seqs)
  featureArray = shapeFeatureArray
  expName = "%s" % shape
  featureNames=[]
  if shape == "shape":
    for shapeType in shapeTypes:
      if shapeType == "MGW" or shapeType == "ProT":
        nCols=seqLen-4
      else:
        nCols=seqLen-3
      featureNames.extend(["%s#%d" % (shapeType, index) for index in xrange(nCols)])
  else:
    featureNames.extend(["%s#%d" % (shape, index) for index in xrange(shapeFeatureArray.shape[1])])

else:
  sys.stderr.write("Error! invalid input parameter combination!\n")
  sys.exit(1)

printTime("Generating features completed")

if options.cvFoldInd is not None and options.cvFoldInd == 0:
  sys.exit() # need to be normal exit
 
# run the svm/svr
if maxFeatureNum is None:
  if bSVR:
    featureNum, scores = svr_fc_nested_cv(featureArray, labels, k_fold=options.cvFold, outputfilehead=options.outputFilename, k_fold_ind=options.cvFoldInd)
  else:
    featureNum, scores = svm_nested_cv(featureArray, labels, k_fold=options.cvFold)
  outputFile.write("%s\t%0.4f (+/-%0.04f)\n" % (expName, np.mean(scores), np.std(scores)/2))
else:
  if bSVR:
    featureNum, scores = svr_fc_nested_cv(featureArray, labels, k_fold=options.cvFold, max_feature_num=maxFeatureNum, feature_names=featureNames,
                                          outputfilehead=options.outputFilename, k_fold_ind=options.cvFoldInd)
  else:
    featureNum, scores = svm_fc_nested_cv(featureArray, labels, k_fold=options.cvFold, max_feature_num=maxFeatureNum, feature_names=featureNames,
                                          outputfilehead=options.outputFilename, k_fold_ind=options.cvFoldInd)
  if featureNum > maxFeatureNum:
    expName = "%s[f%d]" % (expName, maxFeatureNum)
  outputFile.write("%s\t%0.4f (+/-%0.04f)\n" % (expName, np.mean(scores), np.std(scores)/2))

outputFile.close()

printTime("Program ended")
