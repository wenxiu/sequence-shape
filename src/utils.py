import os
import sys
import subprocess
import datetime

#############################################################################
# Run a command with error checking and pipe the stdout to a variable
def pipeCommand(command):
  sys.stderr.write("RUN: %s\n" % command)
  try:
    proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    procReturnCode = proc.wait()
    if (procReturnCode != 0):
      if (procReturnCode == 255):
        # no data in region
        return 0
      else:
        sys.stderr.write("Child was terminated by signal %d\n" % - procReturnCode)
        sys.exit(1)
    else:
      return proc.stdout.read().rstrip()
  except OSError, e:
    sys.stderr.write("Execution failed: %s\n" % e)
    sys.exit(1)

#############################################################################
# Run a command with error checking.
def runCommand(command):
  sys.stderr.write("RUN: %s\n" % command)
  try:
    returnCode = subprocess.call(command, shell=True, executable="/bin/bash")
    if (returnCode != 0):
      sys.stderr.write("Child was terminated by signal %d\n" % -returnCode)
      sys.exit(1)
  except OSError, e:
    sys.stderr.write("Execution failed: %s\n" % e)
    sys.exit(1)

#############################################################################
# print current time

def printTime(msg=None):
  now = datetime.datetime.now()
  sys.stderr.write("%s " % str(now))
  if msg is not None:
    sys.stderr.write("%s\n" % msg )

#############################################################################
# load time string

def loadTime(time_str):
  time_stamp = datetime.datetime.strptime(time_str,'%Y-%m-%d %H:%M:%S.%f')
  return time_stamp

#############################################################################
# convert datedelta to days,hours,minutes

def deltaTime(time_delta):
  d = time_delta.days
  h, remainder = divmod(time_delta.seconds, 3600)
  m, s = divmod(remainder, 60)
  return d,h,m
