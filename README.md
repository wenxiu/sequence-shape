# DNA sequence+shape kernel enables alignment-free modeling of transcription factor binding #

Wenxiu Ma, Lin Yang, Remo Rohs, William S Noble 

### Abstract ###

**Motivation:** Transcription factors (TFs) bind to specific DNA sequence
motifs. Several lines of evidence suggest that TF-DNA binding is mediated in
part by properties of the local DNA shape: the width of the minor groove, the
relative orientations of adjacent base pairs, etc. Several methods have been
developed to jointly account for DNA sequence and shape properties in predicting
TF binding affinity. However, a limitation of these methods is that they
typically require a training set of aligned TF binding sites.  

**Results:** We describe a sequence+shape kernel that leverages DNA sequence and
shape information to better understand protein-DNA binding preference and
affinity.  This kernel extends an existing class of k-mer based sequence
kernels, based on the recently described di-mismatch kernel. Using three in
vitro benchmark datasets, derived from universal protein binding microarrays
(uPBMs), genomic context PBMs (gcPBMs) and SELEX-seq data, we demonstrate that
incorporating DNA shape information improves our ability to predict protein-DNA
binding affinity.  In particular, we observe that (1) the k-spectrum+shape model
performs better than the classical k-spectrum kernel, particularly for small k
values; (2) the di-mismatch kernel performs better than the k-mer kernel, for
larger k; and (3) the di-mismatch+shape kernel performs better than the
di-mismatch kernel for intermediate k values.

### Dependencies ###

The code has been tested using the following python libraries.

- python/2.7.5
- numpy/1.9.3
- scipy/0.16.0
- sklearn/0.16.1

### Usage ###

~~~
$ ./svm-sequence-shape.py -h
Usage: svm-sequence-shape.py [options]

Options:
  -h, --help            show this help message and exit
  -x SEQSFILENAME, --seqfile=SEQSFILENAME
                        input DNA sequence file, one sequence per line
  -y LABELFILENAME, --labelfile=LABELFILENAME
                        [SVM] input label file, one label per line; or [SVR]
                        input affinity file, one affinity value per line.
  -o OUTPUTFILENAME, --outfile=OUTPUTFILENAME
                        output filename
  -r, --svr             OPTIONAL: use SVR regression instead of SVM
                        classification
  -p P, --nt=P          OPTIONAL: use positional k-mer
  -k K, --kmer=K        OPTIONAL: use compositional k-mer
  -m M, --mismatch=M    OPTIONAL: maximum number of mismatches allowed, for
                        di-mismatch or di-mismatch+shape kernel
  -s SHAPE, --shape=SHAPE
                        OPTIONAL: type of DNA shape parameters to use: MGW,
                        Roll, ProT, HelT, shape
  -f MAXFEATURENUM, --feature_selection=MAXFEATURENUM
                        OPTIONAL: maximum number of features to be selected
  -c CVFOLD, --cross_validation_fold=CVFOLD
                        OPTIONAL: c-fold nested cross validation; DEFAULT: 3
  -t CVFOLDIND, --cross_validation_fold_ind=CVFOLDIND
                        OPTIONAL: index of desired outer cross validation
  -d DATADIR, --data-dir=DATADIR
                        OPTIONAL: input database directory for precomputed
                        k-mer data
  -q OUTPUTDIR, --output-dir=OUTPUTDIR
                        OPTIONAL: output results directory
~~~

### Examples ###

Sample scripts can be found in the example/ directory.

- spectrum kernel (k=4)
~~~
mad.rand1000.svr.k4.f1000.job
~~~
- spectrum+shape kernel (k=4)
~~~
mad.rand1000.svr.k4.shape.f1000.job
~~~
- dimismatch kernel (k=4, m=2)
~~~
mad.rand1000.svr.k4.m2.f1000.job
~~~
- di-mismatch+shape kernel (k=4, m=2)
~~~
mad.rand1000.svr.k4.m2.shape.f1000.job
~~~

### Contact us ###

For questions about the use of the sequence+shape kernel code, please contact Wenxiu Ma at [wenxiu.ma@ucr.edu](wenxiu.ma@ucr.edu)
